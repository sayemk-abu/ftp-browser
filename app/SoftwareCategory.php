<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SoftwareCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'software_categories';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'status'];

//    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function softwares()
    {
        return $this->hasMany(Software::class,'category_id','id');
    }

    public function softCount()
    {
        return $this->hasMany(Software::class,'category_id','id')->where('status',1)->count();
    }

}
