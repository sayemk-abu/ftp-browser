<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TvSeriesSeason extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tv_series_seasons';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tv_sesries_id', 'name', 'status','release_date'];

    //use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function tvSeries()
    {
        return $this->belongsTo(TvSeries::class,'tv_sesries_id','id');
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class,'season_id','id');
    }

}
