<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed category_id
 * @property mixed name
 * @property mixed description
 * @property mixed status
 * @property string release_date
 * @property string image
 * @property null file_name
 * @property  has_episode
 * @property  has_season
 */
class TvSeries extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tvseries';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'image', 'description', 'status'];

    //use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function getReleaseDateAttribute($value)
    {
        return date_create_from_format('Y-m-d',$value)->format('d/m/Y');
    }

    /**
     * @return mixed
     */
    public function genres()
    {
        return $this->belongsToMany(Genre::class,'tvseries_genres','tvseries_id','genre_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){
        return $this->belongsTo(TvSeriesCategory::class,'category_id','id');
    }

    public function seasons(){
        return $this->hasMany(TvSeriesSeason::class,'tv_sesries_id','id');
    }

    public function episodes(){
        return $this->hasMany(Episode::class,'tvseries_id','id');

    }




}
