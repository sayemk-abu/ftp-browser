<?php
/**
 * Created by PhpStorm.
 * User: sa
 * Date: 6/14/18
 * Time: 3:51 PM
 */

namespace App\Http;


use Illuminate\Support\Facades\Storage;
use Symfony\Component\Finder\Finder;

class FileBrowser
{

    public static function directories($path = '')
    {
        $directories = Storage::disk('ftp')->directories($path);

        return $directories;
    }

    public static function checkChildren($path='')
    {
        $directory = self::directories($path);

        return (empty($directory)) ? false : true;
    }

    public static function files($path = '')
    {
        return Storage::disk('ftp')->files($path);
    }

    public static function modifiedAt($path = '')
    {
        return Storage::disk('ftp')->lastModified($path);
    }

    public static function byteToMb($bytes=0)
    {
        return round(($bytes/1024)/1024,2);
    }




}