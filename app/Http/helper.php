<?php
/**
 * Created by PhpStorm.
 * User: sa
 * Date: 7/13/18
 * Time: 1:46 AM
 */

function categoryDropdown()
{
    $categories = \App\MovieCategory::orderBy('order','ASC')->where('status',1)->pluck('name','id')->toArray();
    $extra = [0=>'Category'];

    return $extra+$categories;
}

function genreDropdown()
{
    $genres = \App\Genre::where('status',1)->pluck('name','id')->toArray();

    $extra = [0=>'Select Genres'];

    return $extra+$genres;
}

function activeStatus($key)
{
    $statuses = [
        '1'=>'Publish',
        "0"=>'Unpublish'
    ];

    return (isset($key)) ? $statuses[$key] : $statuses;
}

function movieGenresConcate(\App\Movie $movie)
{
    $genreName = '';
    foreach ($movie->genres as $genre)
    {
        $genreName .=$genre->name.', ';
    }

    return trim($genreName,', ');
}

function mostViewed($number = 10)
{
    return $movies = \App\Movie::orderBy('views','DESC')->where('status',1)->take($number)->get();
}

function mostDownloaded($number = 10)
{
    return $movies = \App\Movie::orderBy('downloads','DESC')->where('status',1)->take($number)->get();
}

function recentUpload($number = 10)
{
    return $movies = \App\Movie::orderBy('created_at','DESC')->where('status',1)->take($number)->get();
}

function dateReadable($date)
{
    return @date_create_from_format('d/m/Y',$date)->format('d M Y');
}

function relatedMovies(\App\Movie $movie)
{
    return $movies = \App\Movie::inRandomOrder()
        ->where('status',1)
        ->where('category_id',$movie->category_id)
        ->take(10)
        ->get();
}

function softwareCategoryDropdown()
{
    $genres = \App\SoftwareCategory::where('status',1)->pluck('title','id')->toArray();

    $extra = [0=>'Select Category'];

    return $extra+$genres;
}

function recentUploadSoftware($number = 10)
{
    return $movies = \App\Software::orderBy('created_at','DESC')->where('status',1)->take($number)->get();
}


function gameCategoryDropdown()
{
    $genres = \App\GameCategory::where('status',1)->pluck('name','id')->toArray();

    $extra = [0=>'Select Category'];

    return $extra+$genres;
}

function gamePublisherDropdown()
{
    $genres = \App\GamePublisher::where('status',1)->pluck('name','id')->toArray();

    $extra = [0=>'Select Publisher'];

    return $extra+$genres;
}

function gamePlatformDropdown()
{
    $genres = \App\GamePlatform::where('status',1)->pluck('name','id')->toArray();

    $extra = [0=>'Select Platform'];

    return $extra+$genres;
}

function gamePlatformConcat(\App\Game $game)
{
    $genreName = '';
    foreach ($game->platforms as $platform)
    {
        $genreName .=$platform->name.', ';
    }

    return trim($genreName,', ');
}

function recentUploadGame($number = 10)
{
    return $movies = \App\Game::orderBy('created_at','DESC')->where('status',1)->take($number)->get();
}

function tvSeriesCategoryDropdown()
{
    $genres = \App\TvSeriesCategory::where('status',1)->pluck('name','id')->toArray();

    $extra = [0=>'Select Category'];

    return $extra+$genres;
}

function tvSeriesGenresConcat(\App\TvSeries $tvSeries)
{
    $genreName = '';
    foreach ($tvSeries->genres as $genre)
    {
        $genreName .=$genre->name.', ';
    }

    return trim($genreName,', ');
}


function tvSeriesSessonConcat(\App\TvSeries $tvSeries)
{
    $genreName = '';
    foreach ($tvSeries->seasons as $genre)
    {
        $genreName .=$genre->name.', ';
    }

    return trim($genreName,', ');
}

function tvSeriesDropDownForSeason($status = null)
{
    $tvseries = \App\TvSeries::orderBy('name','ASC')
        ->where('has_season',1)
        ->where(function ($q)use ($status){
            if($status)
                $q->where('status',1);
        })
        ->pluck('name','id')
        ->toArray();
    $extra = [0=>'Select Series'];

    return $extra+$tvseries;
}

function tvSeriesDropDown($status = null)
{
    $tvseries = \App\TvSeries::orderBy('name','ASC')
        ->where(function ($q)use ($status){
            if($status)
                $q->where('status',1);
        })
        ->pluck('name','id')->toArray();
    $extra = [0=>'Select Series'];

    return $extra+$tvseries;
}

function tvSeriesSeasonDropDown($id)
{
    $tvseries = \App\TvSeriesSeason::orderBy('name','ASC')->where('tv_sesries_id',$id)->where('status',1)->pluck('name','id')->toArray();
    $extra = [0=>'Select Season'];

    return $extra+$tvseries;
}

function tvSeriesRecentUpload($number = 10)
{
    return $movies = \App\TvSeries::orderBy('created_at','DESC')->where('status',1)->take($number)->get();
}

function tvSeriesRelated(\App\TvSeries $series)
{
    return \App\TvSeries::inRandomOrder()
        ->where('status',1)
        ->where('category_id',$series->category_id)
        ->take(10)
        ->get();
}

function tvChannelCategoryDropDown($status = null)
{
    $tvseries = \App\TvChannelCategory::orderBy('name','ASC')
        ->where(function ($q)use ($status){
            if($status)
                $q->where('status',1);
        })
        ->pluck('name','id')->toArray();
    $extra = [0=>'Select Series'];

    return $extra+$tvseries;
}

function getMoviesYear($category=null)
{
    return $years = DB::table('movies')
            ->select(DB::raw("YEAR(release_date) as year"))
            ->groupBy(['year'])
            ->where(function ($q)use ($category){
                if($category)
                    $q->where('category_id',$category);
            })
            ->where('status','=',1)
            ->get();
}
