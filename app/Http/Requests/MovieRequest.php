<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|max:512',
            'category_id' =>'bail|required|exists:movie_categories,id',
            'file_size' =>'nullable|max:10',
            'imdb_rating' =>'nullable|numeric',
            'video_quality' =>'nullable|max:100',
            'file_type' =>'nullable|max:100',
            'file_location' =>'required|max:512',
            'duration' => 'nullable|max:20',
            'language' =>'max:100',
            'trailer_link' => 'nullable|max:512|url',
            'image' => 'bail|required|file|mimes:jpeg,bmp,png,jpg',
            'release_date' => 'required|date_format:"d/m/Y"',
            'status' =>'required|boolean',
            'genre_id.*' => 'exists:genres,id'
        ];
    }
}
