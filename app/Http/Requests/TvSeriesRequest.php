<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TvSeriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'category_id'=>'bail|required|exists:tvseriescategories,id',
                    'genre_id.*'=>'bail|required|exists:genres,id',
                    'name' =>'bail|required|unique:tvseries,name',
                    'image' => 'bail|required|file|mimes:jpeg,bmp,png,jpg',
                    'release_date' => 'required|date_format:"d/m/Y"',
                    'status' => 'bail|required|boolean',
                    'file_name' =>'bail|required_with:file_present|max:512',
                    'has_season' => 'bail|required|boolean',
                    'has_episode' => 'bail|required|boolean',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'category_id'=>'bail|required|exists:tvseriescategories,id',
                    'genre_id.*'=>'bail|required|exists:genres,id',
                    'name' =>'bail|required|unique:tvseries,name,'.$this->segment(3),
                    'image' => 'nullable|file|mimes:jpeg,bmp,png,jpg',
                    'release_date' => 'required|date_format:"d/m/Y"',
                    'status' => 'bail|required|boolean',
                    'file_name' =>'bail|required_with:file_present|max:512',
                    'has_season' => 'bail|required|boolean',
                    'has_episode' => 'bail|required|boolean',
                ];
            }
            default:break;
        }
    }


    public function messages()
    {
        return [
            'file_name.required_with'=>'A video file is required when has no season and episode'
        ];
    }
}
