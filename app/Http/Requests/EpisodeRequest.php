<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EpisodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'tvseries_id'=>'bail|required|exists:tvseries,id',
                    'season_id'=>'bail|required_with:has_season|exists:tv_series_seasons,id',
                    'name' =>'bail|required|max:255',
                    'release_date' => 'required|date_format:"d/m/Y"',
                    'status' => 'bail|required|boolean',
                    'file_name' =>'bail|required_with:file_present|max:512',
                    'image' => 'bail|required|file|mimes:jpeg,bmp,png,jpg',

                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'tvseries_id'=>'bail|required|exists:tvseries,id',
                    'season_id'=>'bail|required_with:has_season|exists:tv_series_seasons,id',
                    'name' =>'bail|required|max:255',
                    'release_date' => 'required|date_format:"d/m/Y"',
                    'status' => 'bail|required|boolean',
                    'file_name' =>'bail|required_with:file_present|max:512',
                    'image' => 'bail|file|mimes:jpeg,bmp,png,jpg',

                ];
            }
            default:break;
        }
    }
}
