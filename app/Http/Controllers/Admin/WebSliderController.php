<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\WebSlider;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class WebSliderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $webslider = WebSlider::all();

        return view('backEnd.webslider.index', compact('webslider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.webslider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'image' => 'required|file|mimes:jpeg,bmp,png,jpg',
            'position' => 'required|numeric',
            'link' => 'nullable|url',
            'status' => 'required|boolean',
        ]);

        $path = '';
        if ($request->hasFile('image')) {
            $imageName = 'slide_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('slides', $imageName, 'public');
        }

        $slide = new WebSlider();
        $slide->image = $path;
        $slide->link = $request->link;
        $slide->position = $request->position;
        $slide->status = $request->status;

        if($slide->save())
        {
            flash('Successfully Saved Slider ')->success();
            return redirect()->route('admin.webslider.show',['id'=>$slide->id]);
        }else {
            flash('Failed! to  Save Slider')->error();
        }


        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $webslider = WebSlider::findOrFail($id);

        return view('backEnd.webslider.show', compact('webslider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $webslider = WebSlider::findOrFail($id);

        return view('backEnd.webslider.edit', compact('webslider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $this->validate($request, [
            'image' => 'nullable|file|mimes:jpeg,bmp,png,jpg',
            'position' => 'required|numeric',
            'link' => 'nullable|url',
            'status' => 'required|boolean',
        ]);

        $slide = WebSlider::findOrFail($id);

        $path = '';
        if ($request->hasFile('image')) {
            $imageName = 'slide_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('slides', $imageName, 'public');
        }else {
            $path = $slide->image;
        }


        $slide->image = $path;
        $slide->link = $request->link;
        $slide->position = $request->position;
        $slide->status = $request->status;

        if($slide->save())
        {
            flash('Successfully Updated Slider ')->success();
            return redirect()->route('admin.webslider.show',['id'=>$slide->id]);
        }else {
            flash('Failed! to Update Slider')->error();
        }


        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $webslider = WebSlider::findOrFail($id);

        $webslider->delete();

        Session::flash('message', 'WebSlider deleted!');
        Session::flash('status', 'success');

        return redirect('admin/webslider');
    }

}
