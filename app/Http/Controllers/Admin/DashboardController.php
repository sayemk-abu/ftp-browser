<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\Movie;
use App\Software;
use App\TvSeries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $totalMovies = Movie::count();
        $totalTvSeries = TvSeries::count();
        $totalSoftware = Software::count();
        $totalGame = Game::count();

        $topMovies = Movie::orderBy('views','DESC')->where('status',1)->take(5)->get();
        $topSeries = TvSeries::orderBy('views','DESC')->where('status',1)->take(5)->get();
        $topSoftware = Software::orderBy('downloads','DESC')->where('status',1)->take(5)->get();
        return view('backEnd.dashboard',
            compact('totalMovies','totalTvSeries','totalSoftware',
                'totalGame','topMovies','topSeries','topSoftware'
            ));
    }
}
