<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GameCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class GameCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $gamecategory = GameCategory::all();

        return view('backEnd.gamecategory.index', compact('gamecategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.gamecategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        GameCategory::create($request->all());

        Session::flash('message', 'GameCategory added!');
        Session::flash('status', 'success');

        return redirect('admin/gamecategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gamecategory = GameCategory::findOrFail($id);

        return view('backEnd.gamecategory.show', compact('gamecategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gamecategory = GameCategory::findOrFail($id);

        return view('backEnd.gamecategory.edit', compact('gamecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $gamecategory = GameCategory::findOrFail($id);
        $gamecategory->update($request->all());

        Session::flash('message', 'GameCategory updated!');
        Session::flash('status', 'success');

        return redirect('admin/gamecategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gamecategory = GameCategory::findOrFail($id);

        $gamecategory->delete();

        Session::flash('message', 'GameCategory deleted!');
        Session::flash('status', 'success');

        return redirect('admin/gamecategory');
    }

}
