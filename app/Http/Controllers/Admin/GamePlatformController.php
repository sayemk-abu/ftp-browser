<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GamePlatform;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class GamePlatformController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $gameplatform = GamePlatform::all();

        return view('backEnd.gameplatform.index', compact('gameplatform'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.gameplatform.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        GamePlatform::create($request->all());

        Session::flash('message', 'GamePlatform added!');
        Session::flash('status', 'success');

        return redirect('admin/gameplatform');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gameplatform = GamePlatform::findOrFail($id);

        return view('backEnd.gameplatform.show', compact('gameplatform'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gameplatform = GamePlatform::findOrFail($id);

        return view('backEnd.gameplatform.edit', compact('gameplatform'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $gameplatform = GamePlatform::findOrFail($id);
        $gameplatform->update($request->all());

        Session::flash('message', 'GamePlatform updated!');
        Session::flash('status', 'success');

        return redirect('admin/gameplatform');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gameplatform = GamePlatform::findOrFail($id);

        $gameplatform->delete();

        Session::flash('message', 'GamePlatform deleted!');
        Session::flash('status', 'success');

        return redirect('admin/gameplatform');
    }

}
