<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TvChannel;
use App\TvChannelCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class TvChannelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tvchannel = TvChannel::all();

        return view('backEnd.tvchannel.index', compact('tvchannel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.tvchannel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:tvchannelcategories,id',
            'name' => 'required|max:255',
            'url' => 'required|max:255',
            'image' => 'bail|required|file|mimes:jpeg,bmp,png,jpg',
            ]);
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('tv-channel', $imageName, 'public');
        }

        $channell = new TvChannel();
        $channell->category_id = $request->category_id;
        $channell->name = $request->name;
        $channell->url = $request->url;
        $channell->image = $path;

        if ($channell->save())
        {
            flash('Successfully Saved Channels')->success();
        }else {
            flash('Fail to Save Channels')->error();
        }

        return redirect('admin/tvchannel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tvchannel = TvChannel::findOrFail($id);

        return view('backEnd.tvchannel.show', compact('tvchannel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tvchannel = TvChannel::findOrFail($id);

        return view('backEnd.tvchannel.edit', compact('tvchannel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:tvchannelcategories,id',
            'name' => 'required|max:255',
            'url' => 'required|max:255',
            'image' => 'bail|required|file|mimes:jpeg,bmp,png,jpg',
        ]);
        $channell = TvChannel::findOrFail($id);


        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('tv-series/images', $imageName, 'public');
        } else {
            $path = $channell->image;
        }

        $channell->category_id = $request->category_id;
        $channell->name = $request->name;
        $channell->url = $request->url;
        $channell->image = $path;

        if ($channell->save())
        {
            flash('Successfully Saved Channels')->success();
        }else {
            flash('Fail to Save Channels')->error();
        }


        return redirect('admin/tvchannel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tvchannel = TvChannel::findOrFail($id);

        if ($tvchannel->delete())
        {
            flash('Successfully Delete Channels')->success();
        }else {
            flash('Fail to Delete Channels')->error();
        }

        return redirect('admin/tvchannel');
    }

    public function web()
    {
        $categories = TvChannelCategory::orderBy('name','ASC')->with('channels')->get();


        return view('tv-channel.index',compact('categories'));
    }

    public function watch($id)
    {
        $categories = TvChannelCategory::orderBy('name','ASC')->with('channels')->get();
        $channel = TvChannel::findOrFail($id);
        $serverUrl = 'http://103.81.104.222/live/'.$channel->url.'/index.m3u8';

        return view('tv-channel.watch',compact('categories','channel','serverUrl'));
    }


}
