<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MovieCategory;
use Illuminate\Http\Request;

class MovieCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = MovieCategory::orderBy('order','ASC')->get();

        return view('backEnd.movie-category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backEnd.movie-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'bail|required|max:256|unique:movie_categories,name',
            'order'=>'required|numeric',
            'status'=>"required|boolean"
        ]);
        $movieCategory = new MovieCategory();
        $movieCategory->name = $request->name;
        $movieCategory->status = $request->status;
        $movieCategory->order = $request->order;
        if($movieCategory->save())
            flash('Successfully Save data')->success();
        else
            flash('Fail! to save data')->error();

        return redirect('admin/movie-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MovieCategory  $movieCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MovieCategory $movieCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MovieCategory  $movieCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(MovieCategory $movieCategory)
    {
        return view('backEnd.movie-category.edit',compact('movieCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovieCategory  $movieCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MovieCategory $movieCategory)
    {
        $this->validate($request,[
            'name'=>'bail|required|max:256|unique:movie_categories,name,'.$movieCategory->id,
            'order'=>'required|numeric',
            'status'=>"required|boolean"
        ]);

        $movieCategory->name = $request->name;
        $movieCategory->status = $request->status;
        $movieCategory->order = $request->order;
        if($movieCategory->save())
            flash('Successfully Update data')->success();
        else
            flash('Fail! to update data')->error();

        return redirect('admin/movie-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MovieCategory  $movieCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovieCategory $movieCategory)
    {
        //
    }
}
