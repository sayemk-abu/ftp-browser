<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TvSeriesSeason;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class TvSeriesSeasonController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tvseriesseason = TvSeriesSeason::all();

        return view('backEnd.tvseriesseason.index', compact('tvseriesseason'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.tvseriesseason.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Requests\TvSeriesSeasonRequest $request)
    {


        $season = new TvSeriesSeason();
        $season->tv_sesries_id = $request->tv_sesries_id;
        $season->name = $request->name;
        $season->status = $request->status;
        $season->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');

        if($season->save())
        {
            flash('Successfully Save Season');
            return redirect('admin/tvseriesseason/'.$season->id);
        }else {
            flash('Fail! to Save Season');
            return redirect()->back()->withInput();
        }

        return redirect('admin/tvseriesseason');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tvseriesseason = TvSeriesSeason::findOrFail($id);

        return view('backEnd.tvseriesseason.show', compact('tvseriesseason'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tvseriesseason = TvSeriesSeason::findOrFail($id);

        return view('backEnd.tvseriesseason.edit', compact('tvseriesseason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Requests\TvSeriesSeasonRequest $request)
    {

        $season = TvSeriesSeason::findOrFail($id);
        $season->tv_sesries_id = $request->tv_sesries_id;
        $season->name = $request->name;
        $season->status = $request->status;
        $season->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');

        if($season->save())
        {
            flash('Successfully Update Season');
            return redirect('admin/tvseriesseason/'.$season->id);
        }else {
            flash('Fail! to Update Season');
            return redirect()->back()->withInput();
        }

        return redirect('admin/tvseriesseason');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tvseriesseason = TvSeriesSeason::findOrFail($id);

        $deleteUrl = link_to('admin/tvseriesseason/'.$id.'/delete/confirm', $title = 'Confirm', $attributes = ['class'=>'btn btn-warning btn-xs']);
        flash()->overlay(" This Season may have episodes. By Deleteing, you will loose all episode too.</br>". $deleteUrl, 'Confirm Delete');
        return redirect()->back();

    }

    public function deleteConfirm($id)
    {
        $tvseriesseason = TvSeriesSeason::findOrFail($id);

        foreach ($tvseriesseason->episodes as $episode)
        {
            @Storage::disk('public')->delete('tv-series/'.$episode->file_name);
            $episode->delete();
        }

        if($tvseriesseason->delete())
        {
            flash('Successfuly delete Season')->success();
        }else {
            flash('Fail to delete Season')->error();
        }
        return redirect()->back();
    }

}
