<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TvChannelCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class TvChannelCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tvchannelcategory = TvChannelCategory::all();

        return view('backEnd.tvchannelcategory.index', compact('tvchannelcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.tvchannelcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        TvChannelCategory::create($request->all());

        Session::flash('message', 'TvChannelCategory added!');
        Session::flash('status', 'success');

        return redirect('admin/tvchannelcategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tvchannelcategory = TvChannelCategory::findOrFail($id);

        return view('backEnd.tvchannelcategory.show', compact('tvchannelcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tvchannelcategory = TvChannelCategory::findOrFail($id);

        return view('backEnd.tvchannelcategory.edit', compact('tvchannelcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $tvchannelcategory = TvChannelCategory::findOrFail($id);
        $tvchannelcategory->update($request->all());

        Session::flash('message', 'TvChannelCategory updated!');
        Session::flash('status', 'success');

        return redirect('admin/tvchannelcategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tvchannelcategory = TvChannelCategory::findOrFail($id);

        $tvchannelcategory->delete();

        Session::flash('message', 'TvChannelCategory deleted!');
        Session::flash('status', 'success');

        return redirect('admin/tvchannelcategory');
    }

}
