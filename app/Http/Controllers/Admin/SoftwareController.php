<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Software;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Session;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class SoftwareController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $software = Software::with('category')->get();

        return view('backEnd.software.index', compact('software'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.software.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'category_id' => 'required|exists:software_categories,id',
                'name' => 'required|max:191',
                'image'=>'required|file',
                'file_name'=>'required',
                'status' =>'required|boolean'
            ]);

        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('software/images', $imageName, 'public');
        }

        $software = new Software();
        $software->category_id = $request->category_id;
        $software->name = $request->name;
        $software->image = $path;
        $software->file = $request->file_name;
        $software->status = $request->status;
        $software->description = $request->description;

        if($software->save())
        {
            flash('Successfully Saved Software: '.$request->name)->success();

        }else {
            flash('Failed! to  Save Software: '.$request->name)->error();
        }

        return redirect('admin/software');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $software = Software::findOrFail($id);

        return view('backEnd.software.show', compact('software'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $software = Software::findOrFail($id);

        return view('backEnd.software.edit', compact('software'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:software_categories,id',
            'name' => 'required|max:191',
            'image'=>'file',
            'file_name'=>'required',
            'status' =>'required|boolean'
        ]);
        $software = Software::findOrFail($id);

        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('software/images', $imageName, 'public');
        }else {
            $path = $software->image;
        }


        $software->category_id = $request->category_id;
        $software->name = $request->name;
        $software->image = $path;
        $software->file = $request->file_name;
        $software->status = $request->status;
        $software->description = $request->description;

        if($software->save())
        {
            flash('Successfully Saved Software: '.$request->name)->success();

        }else {
            flash('Failed! to  Save Software: '.$request->name)->error();
        }

        return redirect('admin/software');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $software = Software::findOrFail($id);
        @Storage::disk('public')->delete('software/'.$software->file);


        if($software->delete())
        {
            flash('Successfully delete Software')->success();

        }else {
            flash('Failed! to  delete Software')->error();
        }

        return redirect('admin/software');

    }

    /**
     * Handles the file upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException
     */
    public function upload(Request $request) {
        // create the file receiver
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));
        // check if the upload is success, throw exception or return response you need
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }
        // receive the file
        $save = $receiver->receive();
        // check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            // save the file and return any response you need, current example uses `move` function. If you are
            // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
            return $this->saveFile($save->getFile());
        }
        // we are in chunk mode, lets send the current progress
        /** @var AbstractHandler $handler */
        $handler = $save->handler();
        return response()->json([
            "done" => $handler->getPercentageDone(),
            'status' => true
        ]);
    }

    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(UploadedFile $file)
    {
        $fileName = $this->createFilename($file);
        // Group files by mime type
        $mime = str_replace('/', '-', $file->getMimeType());
        // Group files by the date (week
        //$dateFolder = date("Y-m-W");
        // Build the file path
        $filePath = "public/software";
        $finalPath = storage_path("app/".$filePath);
        // move the file name
        $file->move($finalPath, $fileName);
        return response()->json([
            'path' => $filePath,
            'name' => $fileName,
            'mime_type' => $mime
        ]);
    }
    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension
        $filename = str_replace(" ", "_", $filename);
        // Add timestamp to name of the file
        $filename .= "_" . time() . "." . $extension;
        return $filename;
    }

}
