<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\TvSeriesRequest;
use App\TvSeries;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Session;

use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class TvSeriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tvseries = TvSeries::all();

        return view('backEnd.tvseries.index', compact('tvseries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.tvseries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(TvSeriesRequest $request)
    {
      // return $request->all();
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('tv-series/images', $imageName, 'public');
        }

        $tvSeries = new TvSeries();
        $tvSeries->category_id = $request->category_id;
        $tvSeries->name = $request->name;
        $tvSeries->description = $request->description;
        $tvSeries->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $tvSeries->status = $request->status;
        $tvSeries->image = $path;
        $tvSeries->file_name = ($request->file_present) ? $request->file_name : null;
        $tvSeries->has_episode = $request->has_episode;
        $tvSeries->has_season = $request->has_season;


        if($tvSeries->save())
        {
            $tvSeries->genres()->attach($request->genre_id);
            flash('Successfully Save Series');
            return redirect('admin/tvseries/'.$tvSeries->id);
        }else {
            flash('Fail! to Save Series');
            return redirect()->back()->withInput();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tvseries = TvSeries::findOrFail($id);

        return view('backEnd.tvseries.show', compact('tvseries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tvseries = TvSeries::findOrFail($id);

        return view('backEnd.tvseries.edit', compact('tvseries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, TvSeriesRequest $request)
    {
        $tvSeries = TvSeries::findOrFail($id);
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('tv-series/images', $imageName, 'public');
        } else {
            $path = $tvSeries->image;
        }


        $tvSeries->category_id = $request->category_id;
        $tvSeries->name = $request->name;
        $tvSeries->description = $request->description;
        $tvSeries->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $tvSeries->status = $request->status;
        $tvSeries->image = $path;
        $tvSeries->file_name = ($request->file_present) ? $request->file_name : $tvSeries->file_name;
        $tvSeries->has_episode = $request->has_episode;
        $tvSeries->has_season = $request->has_season;
        if($tvSeries->save())
        {
            $tvSeries->genres()->sync($request->genre_id);
            flash('Successfully Update Series');
            return redirect('admin/tvseries/'.$tvSeries->id);
        }else {
            flash('Fail! to Update Series');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tvseries = TvSeries::findOrFail($id);

        //$tvseries->delete();
        if($tvseries->has_episode)
        {
            $deleteUrl = link_to('admin/tvseries/'.$id.'/delete/confirm', $title = 'Confirm', $attributes = ['class'=>'btn btn-warning btn-xs']);
            flash()->overlay("This TV Series has episode. Please confirm to delete all Episode too. </br>". $deleteUrl, 'Modal Title');
            return redirect()->back();
        }

        if($tvseries->has_season)
        {
            $deleteUrl = link_to('admin/tvseries/'.$id.'/delete/confirm', $title = 'Confirm', $attributes = ['class'=>'btn btn-warning btn-xs']);
            flash()->overlay("This TV Series has season. Please confirm to delete all Season too. </br>". $deleteUrl, 'Modal Title');
            return redirect()->back();
        }

        $tvseries->genres()->detach();
        if($tvseries->delete())
        {
            flash('Successfuly delete tv series')->success();
        }else {
            flash('Fail to delete TV Series')->error();
        }
        return redirect()->back();
    }

    public function deleteConfirm($id)
    {
        $tvseries = TvSeries::findOrFail($id);

        if($tvseries->has_episode)
        {
            foreach ($tvseries->episodes as $episode)
            {
                @Storage::disk('public')->delete('tv-series/'.$episode->file_name);
                $episode->delete();
            }

        }

        if($tvseries->has_seasons)
        {
            foreach ($tvseries->seasons as $season)
            {
                foreach ($season->episodes as $episode)
                {
                    @Storage::disk('public')->delete('tv-series/'.$episode->file_name);
                    $episode->delete();
                }
                $season->delete();
            }

        }

        $tvseries->genres()->detach();
        if($tvseries->delete())
        {
            flash('Successfuly delete tv series')->success();
        }else {
            flash('Fail to delete TV Series')->error();
        }
        return redirect()->back();
    }


    /**
     * Handles the file upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException
     */
    public function upload(Request $request) {

        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));

        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        $save = $receiver->receive();

        if ($save->isFinished()) {

            return $this->saveFile($save->getFile());
        }


        $handler = $save->handler();
        return response()->json([
            "done" => $handler->getPercentageDone(),
            'status' => true
        ]);
    }

    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(UploadedFile $file)
    {
        $fileName = $this->createFilename($file);
        // Group files by mime type
        $mime = str_replace('/', '-', $file->getMimeType());
        // Group files by the date (week
        //$dateFolder = date("Y-m-W");
        // Build the file path
        $filePath = "public/tv-series";
        $finalPath = storage_path("app/".$filePath);
        // move the file name
        $file->move($finalPath, $fileName);
        return response()->json([
            'path' => $filePath,
            'name' => $fileName,
            'mime_type' => $mime
        ]);
    }
    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension
        $filename = str_replace(" ", "_", $filename);
        // Add timestamp to name of the file
        $filename .= "_" . time() . "." . $extension;
        return $filename;
    }


    public function season($id)
    {
        $tvSeries = TvSeries::findOrFail($id);
        $response = [
            'has_season'=>$tvSeries->has_season,
            'seasons' => tvSeriesSeasonDropDown($id)
        ];
        return response()->json($response);
    }

}
