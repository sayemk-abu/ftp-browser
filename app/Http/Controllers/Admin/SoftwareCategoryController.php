<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SoftwareCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class SoftwareCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $softwarecategory = SoftwareCategory::all();

        return view('backEnd.admin.softwarecategory.index', compact('softwarecategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.admin.softwarecategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', ]);

        SoftwareCategory::create($request->all());

        Session::flash('message', 'SoftwareCategory added!');
        Session::flash('status', 'success');

        return redirect('admin/softwarecategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $softwarecategory = SoftwareCategory::findOrFail($id);

        return view('backEnd.admin.softwarecategory.show', compact('softwarecategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $softwarecategory = SoftwareCategory::findOrFail($id);

        return view('backEnd.admin.softwarecategory.edit', compact('softwarecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['title' => 'required', ]);

        $softwarecategory = SoftwareCategory::findOrFail($id);
        $softwarecategory->update($request->all());

        Session::flash('message', 'SoftwareCategory updated!');
        Session::flash('status', 'success');

        return redirect('admin/softwarecategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $softwarecategory = SoftwareCategory::findOrFail($id);

        $softwarecategory->delete();

        Session::flash('message', 'SoftwareCategory deleted!');
        Session::flash('status', 'success');

        return redirect('admin/softwarecategory');
    }

}
