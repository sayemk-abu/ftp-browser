<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GamePublisher;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class GamePublisherController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $gamepublisher = GamePublisher::all();

        return view('backEnd.gamepublisher.index', compact('gamepublisher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.gamepublisher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        GamePublisher::create($request->all());

        Session::flash('message', 'GamePublisher added!');
        Session::flash('status', 'success');

        return redirect('admin/gamepublisher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gamepublisher = GamePublisher::findOrFail($id);

        return view('backEnd.gamepublisher.show', compact('gamepublisher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gamepublisher = GamePublisher::findOrFail($id);

        return view('backEnd.gamepublisher.edit', compact('gamepublisher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $gamepublisher = GamePublisher::findOrFail($id);
        $gamepublisher->update($request->all());

        Session::flash('message', 'GamePublisher updated!');
        Session::flash('status', 'success');

        return redirect('admin/gamepublisher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gamepublisher = GamePublisher::findOrFail($id);

        $gamepublisher->delete();

        Session::flash('message', 'GamePublisher deleted!');
        Session::flash('status', 'success');

        return redirect('admin/gamepublisher');
    }

}
