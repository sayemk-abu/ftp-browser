<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TvSeriesCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class TvSeriesCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tvseriescategory = TvSeriesCategory::all();

        return view('backEnd.tvseriescategory.index', compact('tvseriescategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.tvseriescategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        TvSeriesCategory::create($request->all());

        Session::flash('message', 'TvSeriesCategory added!');
        Session::flash('status', 'success');

        return redirect('admin/tvseriescategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tvseriescategory = TvSeriesCategory::findOrFail($id);

        return view('backEnd.tvseriescategory.show', compact('tvseriescategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tvseriescategory = TvSeriesCategory::findOrFail($id);

        return view('backEnd.tvseriescategory.edit', compact('tvseriescategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $tvseriescategory = TvSeriesCategory::findOrFail($id);
        $tvseriescategory->update($request->all());

        Session::flash('message', 'TvSeriesCategory updated!');
        Session::flash('status', 'success');

        return redirect('admin/tvseriescategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tvseriescategory = TvSeriesCategory::findOrFail($id);

        $tvseriescategory->delete();

        Session::flash('message', 'TvSeriesCategory deleted!');
        Session::flash('status', 'success');

        return redirect('admin/tvseriescategory');
    }

}
