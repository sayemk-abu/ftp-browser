<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Episode;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class EpisodeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $episode = Episode::where(function ($q)use ($request){
            if($request->season)
            {
                $q->where('season_id',$request->season);
            }
            if($request->tvserie)
            {
                $q->where('tvseries_id',$request->tvserie);
            }
        })->get();

        return view('backEnd.episode.index', compact('episode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.episode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Requests\EpisodeRequest $request)
    {
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('tv-series/images', $imageName, 'public');
        }
        $episode = new Episode();
        $episode->name = $request->name;
        $episode->tvseries_id = $request->tvseries_id;
        $episode->season_id = $request->season_id;
        $episode->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $episode->status = $request->status;
        $episode->file_name = $request->file_name;
        $episode->image = $path;

        if($episode->save())
        {
            flash('Successfully Save Episode');
            return redirect('admin/episode/'.$episode->id);
        }else {
            flash('Fail! to Save Episode');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $episode = Episode::findOrFail($id);

        return view('backEnd.episode.show', compact('episode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $episode = Episode::findOrFail($id);

        return view('backEnd.episode.edit', compact('episode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $episode = Episode::findOrFail($id);

        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('tv-series/images', $imageName, 'public');
        } else {
            $path = $episode->image;
        }

        $episode->name = $request->name;
        $episode->tvseries_id = $request->tvseries_id;
        $episode->season_id = $request->season_id;
        $episode->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $episode->status = $request->status;
        $episode->file_name = $request->file_name;
        $episode->image = $path;

        if($episode->save())
        {
            flash('Successfully Update Episode');
            return redirect('admin/episode/'.$episode->id);
        }else {
            flash('Fail! to Update Episode');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $episode = Episode::findOrFail($id);

        @Storage::disk('public')->delete('tv-series/'.$episode->file_name);


        if($episode->delete())
        {
            flash('Successfuly delete Episode')->success();
        }else {
            flash('Fail to delete Episode')->error();
        }

        return redirect('admin/episode');
    }

}
