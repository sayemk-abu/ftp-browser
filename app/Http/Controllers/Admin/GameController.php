<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Game;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Session;

use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class GameController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $game = Game::all();

        return view('backEnd.game.index', compact('game'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.game.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'category_id' => 'required|exists:gamecategories,id',
            'publisher_id' => 'required|exists:gamepublishers,id',
            'platform.*' => 'required|exists:gameplatforms,id',
            'name' => 'required|max:191',
            'file_location' => 'required',
            'file_size' => 'required',
            'image' => 'required|file',
            'release_date' => 'required|date_format:"d/m/Y"',
            'status' =>'required|boolean',
        ]);
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('game/images', $imageName, 'public');
        }

        $game = new Game();
        $game->category_id = $request->category_id;
        $game->publisher_id = $request->publisher_id;
        $game->name = $request->name;
        $game->file_location = $request->file_location;
        $game->file_size = $request->file_size;
        $game->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $game->status = $request->status;
        $game->image = $path;
        $game->description = $request->description;

        if($game->save())
        {
            $game->platforms()->attach($request->platform);

            flash('Successfully Saved Game: '.$request->name)->success();
            return redirect()->route('admin.game.show',['id'=>$game->id]);
        }else {
            flash('Failed! to  Save Game: '.$request->name)->error();
        }

        return redirect()->back()->withInput($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $game = Game::findOrFail($id);

        return view('backEnd.game.show', compact('game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $game = Game::findOrFail($id);

        return view('backEnd.game.edit', compact('game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:gamecategories,id',
            'publisher_id' => 'required|exists:gamepublishers,id',
            'platform.*' => 'required|exists:gameplatforms,id',
            'name' => 'required|max:191',
            'file_location' => 'required',
            'file_size' => 'required',
            'image' => 'nullable|file',
            'release_date' => 'required|date_format:"d/m/Y"',
            'status' =>'required|boolean',
        ]);
        $game = Game::findOrFail($id);
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('game/images', $imageName, 'public');
        }else {
            $path = $game->image;
        }

        $game->category_id = $request->category_id;
        $game->publisher_id = $request->publisher_id;
        $game->name = $request->name;
        $game->file_location = $request->file_location;
        $game->file_size = $request->file_size;
        $game->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $game->status = $request->status;
        $game->image = $path;
        $game->description = $request->description;

        if($game->save())
        {
            $game->platforms()->sync($request->platform);

            flash('Successfully Saved Game: '.$request->name)->success();
            return redirect()->route('admin.game.show',['id'=>$game->id]);
        }else {
            flash('Failed! to  Save Game: '.$request->name)->error();
        }

        return redirect()->back()->withInput($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $game = Game::findOrFail($id);

        @Storage::disk('public')->delete('game/'.$game->file_location);
        @Storage::disk('public')->delete($game->image);
        $game->platforms()->detach();

        if($game->delete())
        {
            flash('Successfully delete Game')->success();

        }else {
            flash('Failed! to  delete Game')->error();
        }

        return redirect('admin/game');
    }

    /**
     * Handles the file upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException
     */
    public function upload(Request $request) {
        // create the file receiver
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));
        // check if the upload is success, throw exception or return response you need
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }
        // receive the file
        $save = $receiver->receive();
        // check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            // save the file and return any response you need, current example uses `move` function. If you are
            // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
            return $this->saveFile($save->getFile());
        }
        // we are in chunk mode, lets send the current progress
        /** @var AbstractHandler $handler */
        $handler = $save->handler();
        return response()->json([
            "done" => $handler->getPercentageDone(),
            'status' => true
        ]);
    }

    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(UploadedFile $file)
    {
        $fileName = $this->createFilename($file);
        // Group files by mime type
        $mime = str_replace('/', '-', $file->getMimeType());
        // Group files by the date (week
        //$dateFolder = date("Y-m-W");
        // Build the file path
        $filePath = "public/game";
        $finalPath = storage_path("app/".$filePath);
        // move the file name
        $file->move($finalPath, $fileName);
        return response()->json([
            'path' => $filePath,
            'name' => $fileName,
            'mime_type' => $mime
        ]);
    }
    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension
        $filename = str_replace(" ", "_", $filename);
        // Add timestamp to name of the file
        $filename .= "_" . time() . "." . $extension;
        return $filename;
    }

}
