<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MovieRequest;
use App\Http\Requests\UpdateMoveRequest;
use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $movies = Movie::all();
        return view('backEnd.movies.index',compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backEnd.movies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieRequest $request)
    {
        //return $request->all();
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('images', $imageName, 'public');
        }


        $movie = new Movie();
        $movie->name = $request->name;
        $movie->category_id = $request->category_id;
        $movie->file_size = $request->file_size;
        $movie->imdb_rating = $request->imdb_rating;
        $movie->video_quality = $request->video_quality;
        $movie->file_type = $request->file_type;
        $movie->file_location = $request->file_location;
        $movie->duration = $request->duration;
        $movie->language = $request->language;
        $movie->trailer_link = $request->trailer_link;
        $movie->image = $path;
        $movie->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $movie->status = $request->status;

        //
        $movie->views = 0;
        $movie->downloads = 0;



        if($movie->save())
        {
            $movie->genres()->sync($request->genre_id);

            flash('Successfully Saved Movie: '.$request->name)->success();
            return redirect()->route('admin.movies.show',['id'=>$movie->id]);
        }else {
            flash('Failed! to  Save Movie: '.$request->name)->error();
        }

        return redirect()->back()->withInput($request->all());


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return view('backEnd.movies.show',compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {

        return view('backEnd.movies.edit',compact('movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMoveRequest $request, Movie $movie)
    {
        $path = '';
        if ($request->hasFile('image')) {
            $imageName = str_replace(" ",'_',$request->name).'_'.time().'.'.$request->image->extension();
            $path = $request->image->storeAs('images', $imageName, 'public');
        }else {
            $path = $movie->image;
        }



        $movie->name = $request->name;
        $movie->category_id = $request->category_id;
        $movie->file_size = $request->file_size;
        $movie->imdb_rating = $request->imdb_rating;
        $movie->video_quality = $request->video_quality;
        $movie->file_type = $request->file_type;
        $movie->file_location = $request->file_location;
        $movie->duration = $request->duration;
        $movie->language = $request->language;
        $movie->trailer_link = $request->trailer_link;
        $movie->image = $path;
        $movie->release_date = date_create_from_format('d/m/Y',$request->release_date)->format('Y-m-d');
        $movie->status = $request->status;


        if($movie->save())
        {
            $movie->genres()->sync($request->genre_id);

            flash('Successfully Updated Movie: '.$request->name)->success();
            return redirect()->route('admin.movies.show',['id'=>$movie->id]);
        }else {
            flash('Failed! to  Update Movie: '.$request->name)->error();
        }

        return redirect()->back()->withInput($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        @Storage::disk('public')->delete($movie->file_location);


        if($movie->delete())
        {
            flash('Successfully delete Movie')->success();

        }else {
            flash('Failed! to  delete Movie')->error();
        }

        return redirect('admin/movies');
    }
}
