<?php

namespace App\Http\Controllers;

use App\Game;
use App\GameCategory;
use App\GamePublisher;
use Illuminate\Http\Request;

class FrontGameController extends Controller
{

    public function index(Request $request)
    {
        $games = Game::orderBy('downloads','DESC')->where(function ($q) use($request){
           if($request->category)
           {
               $q->where('category_id',$request->category);
           }
            if($request->publisher)
            {
                $q->where('publisher_id',$request->publisher);
            }
            if ($request->search)
            {
                $q->where('name','LIKE',"%$request->search%");
            }
        })->paginate(9);

        $games->appends(request()->except(['page','_token']));
        $categories =  $this->categoryMovieCount();
        $publishers = $this->publisherMovieCount();
//        $totalGame = $this->allGamesCount();

        if($request->category || $request->publisher || $request->search)
        {
            $pageTitle = 'Game Search Result';
        }else {
            $pageTitle = 'Games';
        }

        return view('game.index',compact('games','categories','publishers','totalGame','pageTitle'));


    }


    public function show($id)
    {
        $game = Game::findOrFail($id);

        return view('game.show',compact('game'));

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $movie = Game::findOrFail($id);
        $movie->increment('downloads');

        return response()->download('storage/game/'.$movie->file_location);
    }

    private function categoryMovieCount()
    {
        $categories =  GameCategory::where('status',1)->get();
        $cats = [];
        foreach ($categories as $category)
        {
            $category->gameCount = $category->gameCount();
            if($category->gameCount > 0) {
                $cats[] = $category;
            }


        }

        return $cats;
    }


    private function publisherMovieCount()
    {
        $categories =  GamePublisher::where('status',1)->get();
        $cats = [];
        foreach ($categories as $category)
        {
            $category->gameCount = $category->gameCount();
            if($category->gameCount > 0) {
                $cats[] = $category;
            }


        }

        return $cats;
    }

    private function allGamesCount()
    {
        return Game::where('status',1)->count();
    }
}
