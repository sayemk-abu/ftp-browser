<?php

namespace App\Http\Controllers;

use App\Software;
use App\SoftwareCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class FrontSoftwareController extends Controller
{
    public function index(Request $request)
    {
//        return SoftwareCategory::where('status',1)->get()[0]->softCount();


        $softwares = Software::where(function ($q) use ($request) {
           if($request->category)
           {
               $q->where('category_id',$request->category);
           }

           if ($request->name)
           {
               $q->where('name','LIKE',"%$request->name%");
           }

           $q->where('status',1);
        })->paginate(9);

        $pageTitle = 'Software';

        $softwares->appends(request()->except(['page','_token']));
        $categories =  $this->categorySoftwareCount();

        $totalSoft = $this->allSoftwareCount();

        return view('software.index',compact('softwares','pageTitle','categories','totalSoft'));
    }

    private function categorySoftwareCount()
    {
        $categories =  SoftwareCategory::where('status',1)->get();
        $cats = [];
        foreach ($categories as $category)
        {
            $category->softCount = $category->softCount();
            if($category->softCount > 0) {
                $cats[] = $category;
            }


        }

        return $cats;
    }

    private function allSoftwareCount()
    {
        return Software::where('status',1)->count();
    }

    public function download($id)
    {
        try {
            $software = Software::findOrFail($id);
            $software->increment('downloads');
            return response()->download('storage/software/'.$software->file);
        }catch (FileNotFoundException $exception)
        {
            Log::info($exception->getMessage());
            abort(501);
        }
    }
}
