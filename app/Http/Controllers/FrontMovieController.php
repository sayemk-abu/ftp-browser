<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Movie;
use App\MovieCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FrontMovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function genre(Request $id)
    {

    }


    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

        Log::info($request->url());
        //sleep(5);


        $movie = Movie::findOrFail($id);
        if($movie->status ==0)
        {
            abort(404);
        }

        $movie->increment('views');

        return view('movie.show',compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
//        return getMoviesYear($request->category);
        if($request->genre)
        {

             $movies = DB::table('movies')->select(['movies.*'])
                ->orderBy('views','DESC')
                ->where('status','=',1)
                ->where('movie_genres.genre_id',$request->genre)
                ->join('movie_genres','movie_genres.movie_id','=','movies.id')
                ->paginate(9);

        }else {
            $movies = Movie::orderBy('views','DESC')->where('status','=',1)
                ->where(function ($q) use($request){
                    if(!empty($request->category))
                    {
                        $q->where('category_id',$request->category);
                    }
                    if(!empty($request->year))
                    {
                        $q->whereYear('release_date',$request->year);
                    }
                    if(!empty($request->movie))
                    {
                        $q->where('name','LIKE',"%$request->movie%");

                    }

                })

                ->paginate(9);
        }
        $movies->appends(request()->except(['page','_token']));
        $categories =  $this->categoryMovieCount();

        $totalMovies = $this->allMoviesCount();
        $topMovie = $movies->first();
        if(empty($topMovie))
        {
            $topMovie = Movie::orderBy('views','DESC')->where('status','=',1)->first();
        }

        if($request->category || $request->genre || $request->movie)
        {
            $pageTitle = 'Search Result';
        }else {
            $pageTitle = 'Movies';
        }

        if($request->category)
        {
            $yearUrl = "category=".$request->category." &";
        }else {
            $yearUrl = '';
        }

        $releaseYears = getMoviesYear($request->category);

        return view('movie.search',compact('movies','pageTitle','categories','totalMovies','topMovie','releaseYears','yearUrl'));
    }


    public function recentUpload()
    {
        $movies = Movie::orderBy('created_at','DESC')->where('status','=',1)

            ->paginate(20);

        return view('movie.recent',compact('movies'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->increment('downloads');

        return response()->download('storage/'.$movie->file_location);
    }

    private function categoryMovieCount()
    {
        $categories =  MovieCategory::where('status',1)->get();
        $cats = [];
        foreach ($categories as $category)
        {
            $category->movieCounts = $category->movieCount();
            if($category->movieCounts > 0) {
                $cats[] = $category;
            }


        }

        return $cats;
    }

    private function allMoviesCount()
    {
        return Movie::where('status',1)->count();
    }
}
