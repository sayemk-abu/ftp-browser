<?php

namespace App\Http\Controllers;

use App\Episode;
use App\TvSeries;
use App\TvSeriesCategory;
use Illuminate\Http\Request;

class FrontTvSeriesController extends Controller
{

    public function index(Request $request)
    {
        $tvSeries = TvSeries::orderBy('release_date','DESC')->where(function ($q) use ($request){
           $q->where('status',1);
           if($request->category)
           {
               $q->where('category_id',$request->category);
           }
        })->paginate(12);


        $tvSeries->appends(request()->except(['page','_token']));
        $categories =  $this->categorySeriesCount();

        $totalSeries = $this->allSeriesCount();


        if($request->category || $request->genre || $request->movie)
        {
            $pageTitle = 'Search Result';
        }else {
            $pageTitle = 'Tv Series';
        }

        return view('tvseries.index',compact('tvSeries','pageTitle','categories','totalSeries'));
    }

    public function show($id)
    {
        $series = TvSeries::findOrFail($id);
        if($series->has_season)
        {
            $series->seasons = $series->seasons()->with('episodes')->get();
            return view('tvseries.season',compact('series'));
        }

        if($series->has_episode)
        {
            $series->episodes;

            $episode = $series->episodes->first();

            return view('tvseries.episode',compact('series','episode'));
        }
        $series->increment('views');

        return view('tvseries.show',compact('series'));
    }

    public function watch($tvSeriesId,$seasonId,$episodeId)
    {
        $episode = Episode::findOrFail($episodeId);
        $series = TvSeries::findOrFail($tvSeriesId);
        $series->increment('views');
        $episode->increment('views');
        if($series->has_season)
        {
            $series->seasons = $series->seasons()->with('episodes')->get();
        }

        return view('tvseries.season_watch',compact('episode','series'));

    }

    public function episodeWatch($id, $episodeId)
    {
        $series = TvSeries::findOrFail($id);
        $episode = Episode::findOrFail($episodeId);
        $series->increment('views');
        $episode->increment('views');

        return view('tvseries.episode',compact('series','episode'));
    }

    public function download($id,$name,$episodeId =null)
    {
        $series = TvSeries::findOrFail($id);
        $series->increment('downloads');
        if($episodeId)
        {
            $episode = Episode::findOrFail($episodeId);
            $episode->increment('downloads');

            return response()->download('storage/tv-series/'.$episode->file_name);
        }

        return response()->download('storage/tv-series/'.$series->file_name);
    }

    private function categorySeriesCount() {
        $categories =  TvSeriesCategory::where('status',1)->get();
        $cats = [];
        foreach ($categories as $category)
        {
            $category->seriesCounts = $category->series()->where('status',1)->count();
            if($category->seriesCounts > 0) {
                $cats[] = $category;
            }


        }

        return $cats;
    }

    private function allSeriesCount(){
        return TvSeries::where('status',1)->count();
    }
}
