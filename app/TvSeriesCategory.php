<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TvSeriesCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tvseriescategories';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function series(){
        return $this->hasMany(TvSeries::class,'category_id','id');
    }

}
