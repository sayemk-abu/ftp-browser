<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TvChannelCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tvchannelcategories';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function channels() {
        return $this->hasMany(TvChannel::class,'category_id','id');
    }

}
