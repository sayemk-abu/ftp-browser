<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebSlider extends Model
{
    protected $table = 'web_sliders';

    protected $fillable = ['image','link','position','status'];
}
