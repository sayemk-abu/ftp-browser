<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genres';

    public function movies()
    {
        return $this->belongsToMany(Movie::class,'movie_genres','movie_id','genre_id');
    }
}
