<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Episode extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'episodes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status'];

  //  use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function getReleaseDateAttribute($value)
    {
        return date_create_from_format('Y-m-d',$value)->format('d/m/Y');
    }

    public function tvseries()
    {
        return $this->belongsTo(TvSeries::class,'tvseries_id','id');
    }

    public function season(){
        return $this->belongsTo(TvSeriesSeason::class,'season_id','id');
    }


}
