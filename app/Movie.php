<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed release_date
 * @property mixed name
 * @property mixed category_id
 * @property mixed file_size
 * @property mixed imdb_rating
 * @property mixed video_quality
 * @property mixed file_type
 * @property mixed file_location
 * @property mixed duration
 * @property mixed language
 * @property mixed trailer_link
 * @property string image
 * @property mixed status
 */
class Movie extends Model
{

    protected $table = 'movies';

    public function genres()
    {
        return $this->belongsToMany(Genre::class,'movie_genres','movie_id','genre_id');
    }

    public function category() {
        return $this->belongsTo(MovieCategory::class,'category_id','id');
    }


    public function getReleaseDateAttribute($value)
    {
        return date_create_from_format('Y-m-d',$value)->format('d/m/Y');
    }

}
