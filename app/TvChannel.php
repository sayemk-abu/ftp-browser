<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TvChannel extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tvchannels';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'name', 'url', 'image', 'status'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function category() {
        return $this->belongsTo(TvChannelCategory::class,'category_id','id');
    }

}
