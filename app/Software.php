<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Software extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'software';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'name', 'description', 'status','file','image'];

    public function category()
    {
        return $this->belongsTo(SoftwareCategory::class,'category_id','id');
    }

//    use SoftDeletes;
    protected $dates = ['deleted_at'];

}
