<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gamecategories';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status'];

//    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function gameCount()
    {
        return $this->hasMany(Game::class,'category_id','id')->count();
    }

    public function movies()
    {
        return $this->hasMany(Game::class,'category_id','id');
    }


}
