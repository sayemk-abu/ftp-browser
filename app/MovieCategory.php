<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieCategory extends Model
{
    protected $table = 'movie_categories';

    public function movies()
    {
        return $this->hasMany(Movie::class,'category_id','id');
    }

    public function movieCount()
    {
        return $this->hasMany(Movie::class,'category_id','id')->count();
    }
}
