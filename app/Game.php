<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'games';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'publisher_id', 'platform', 'name', 'file_location', 'file_size', 'image', 'description', 'status','release_date'];

//    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function category(){
        return $this->belongsTo(GameCategory::class,'category_id','id');
    }

    public function publisher()
    {
        return $this->belongsTo(GamePublisher::class,'publisher_id','id');
    }

    public function platforms()
    {
        return $this->belongsToMany(GamePlatform::class,'game_platform_relations','platform_id','game_id');
    }

    public function getReleaseDateAttribute($value)
    {
        return date_create_from_format('Y-m-d',$value)->format('d/m/Y');
    }

}
