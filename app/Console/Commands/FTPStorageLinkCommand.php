<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FTPStorageLinkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:link-ftp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symbolic link from "public/ftp" to "storage/app/ftp"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(public_path('ftp'))) {
            return $this->error('The "public/ftp" directory already exists.');
        }

        $this->laravel->make('files')->link(
            storage_path('app/ftp'), public_path('ftp')
        );

        $this->info('The [public/ftp] directory has been linked.');
    }
}
