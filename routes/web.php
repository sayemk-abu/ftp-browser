<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses'=>'Admin\\TvChannelController@web','as'=>'web.welcome']);
//Route::get('info', function () {
//    phpinfo();
//});
Auth::routes();
Route::get('/movie',['uses'=>'FrontMovieController@search','as'=>'web.movie']);
Route::get('/movie/{id}/{name}',['uses'=>'FrontMovieController@show','as'=>'web.movie.show']);
Route::get('/movie/{id}/download/{name}',['uses'=>'FrontMovieController@download','as'=>'web.movie.download']);
Route::get('/movie/search',['uses'=>'FrontMovieController@search','as'=>'web.movie.search']);
Route::get('/movie/recent',['uses'=>'FrontMovieController@recentUpload','as'=>'web.movie.recent']);

Route::get('/software',['uses'=>'FrontSoftwareController@index','as'=>'web.software.index']);
Route::get('/software/{id}/{name}',['uses'=>'FrontSoftwareController@download','as'=>'web.software.show']);
Route::get('/software/download/{id}/{name}',['uses'=>'FrontSoftwareController@download','as'=>'web.software.download']);

Route::get('/game',['uses'=>'FrontGameController@index','as'=>'web.game.index']);
Route::get('/game/{id}/{name}',['uses'=>'FrontGameController@show','as'=>'web.game.show']);
Route::get('/game/download/{id}/{name}',['uses'=>'FrontGameController@download','as'=>'web.game.download']);

Route::get('/tv-series',['uses'=>'FrontTvSeriesController@index','as'=>'web.series.index']);
Route::get('/tv-series/{id}/{name}',['uses'=>'FrontTvSeriesController@show','as'=>'web.series.show']);
Route::get('/tv-series/download/{id}/{name}/{episode?}',['uses'=>'FrontTvSeriesController@download','as'=>'web.series.download']);
Route::get('/tv-series/watch/{id}/season/{name}/episode/{episode}',['uses'=>'FrontTvSeriesController@watch','as'=>'web.series.watch']);
Route::get('/tv-series/watch/{id}/episode/{episode}',['uses'=>'FrontTvSeriesController@episodeWatch','as'=>'web.series.episode.watch']);
Route::get('/live-tv',['uses'=>'Admin\\TvChannelController@web','as'=>'web.live-tv']);
Route::get('/live-tv/{id}/{name}',['uses'=>'Admin\\TvChannelController@watch','as'=>'web.tv-channel.watch']);
Route::get('/home', 'HomeController@index')->name('home')->middleware(['web','auth']);
Route::namespace('Admin')->middleware(['web', 'auth'])->name('admin.')->prefix('admin')->group(function () {
    Route::get('dashboard', 'DashboardController@dashboard');
    Route::resource('movie-category', 'MovieCategoryController');
    Route::resource('genre', 'GenreController');
    Route::resource('movies', 'MovieController');
    Route::post('upload/movie', 'UploadController@upload');

    Route::resource('webslider', 'WebSliderController');

    Route::resource('softwarecategory', 'SoftwareCategoryController');
    Route::post('software/upload', 'SoftwareController@upload');
    Route::resource('software', 'SoftwareController');

    Route::post('game/upload', 'GameController@upload');
    Route::resource('gamecategory', 'GameCategoryController');
    Route::resource('gameplatform', 'GamePlatformController');
    Route::resource('gamepublisher', 'GamePublisherController');
    Route::resource('game', 'GameController');
    Route::resource('tvseriescategory', 'TvSeriesCategoryController');
    Route::resource('episode', 'EpisodeController');
    Route::post('tvseries/upload', 'TvSeriesController@upload');
    Route::get('tvseries/season/{id}', 'TvSeriesController@season');
    Route::get('/tvseries/{id}/delete/confirm','TvSeriesController@deleteConfirm');
    Route::resource('tvseries', 'TvSeriesController');

    Route::get('/tvseriesseason/{id}/delete/confirm','TvSeriesSeasonController@deleteConfirm');
    Route::resource('tvseriesseason', 'TvSeriesSeasonController');

    Route::resource('tvchannelcategory', 'TvChannelCategoryController');
    Route::resource('tvchannel', 'TvChannelController');

});
