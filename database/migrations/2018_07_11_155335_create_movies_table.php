<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',512)->index();
            $table->unsignedInteger('category_id')->index();
            $table->string('file_size',10);
            $table->float('imdb_rating',5,2)->index()->nullable();
            $table->string('video_quality',100)->index();
            $table->string('file_type',100)->nullable();
            $table->string('file_location',512)->nullable();
            $table->string('duration',20)->nullable();
            $table->string('language',100)->nullable();
            $table->string('trailer_link',512)->nullable();
            $table->string('image',512)->nullable();
            $table->text('description',512)->nullable();
            $table->date('release_date')->nullable();
            $table->string('subtitle_link',512)->nullable();
            $table->unsignedInteger('views')->defualt(0)->index();
            $table->unsignedInteger('downloads')->defualt(0)->index();
            $table->boolean('status')->defualt(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
