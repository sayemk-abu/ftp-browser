<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('episodes', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->unsignedInteger('tvseries_id')->index()->nullable();
                $table->unsignedInteger('season_id')->index()->nullable();
                $table->string('file_name',512);
                $table->unsignedInteger('views')->default(0);
                $table->unsignedInteger('downloads')->default(0);
                $table->date('release_date')->nullable();
                $table->boolean('status')->index();

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('episodes');
    }

}
