<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTvseriesseasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('tv_series_seasons', function(Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('tv_sesries_id')->index();
                $table->string('name');
                $table->date('release_date')->nullable();
                $table->boolean('status');

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tv_series_seasons');
    }

}
