<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSoftwareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('software', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('category_id')->index();
                $table->string('name',191)->index();
                $table->string('file',1024)->nullable();
                $table->string('image',1024)->nullable();
                $table->text('description')->nullable();
                $table->boolean('status');

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('software');
    }

}
