<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('games', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('category_id')->index();
                $table->integer('publisher_id')->index();
                $table->string('name')->index();
                $table->string('file_location');
                $table->string('file_size');
                $table->string('image');
                $table->date('release_date')->nullable();
                $table->unsignedInteger('downloads')->default(0)->index();
                $table->longText('description')->nullable();
                $table->boolean('status')->index();

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('games');
    }

}
