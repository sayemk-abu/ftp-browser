<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTvseriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('tvseries', function(Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('category_id')->index();
                $table->string('name',512)->index();
                $table->string('image')->nullable();
                $table->text('description')->nullable();
                $table->unsignedInteger('views')->default(0);
                $table->unsignedInteger('downloads')->default(0);
                $table->date('release_date')->nullable();
                $table->boolean('has_season')->default(1);
                $table->boolean('has_episode')->default(0);
                $table->string('file_name',512)->nullable();
                $table->boolean('status')->index();

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tvseries');
    }

}
