<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTvchannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('tvchannels', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('category_id')->index();
                $table->string('name');
                $table->string('url');
                $table->string('image');
                $table->boolean('status')->default(1);

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tvchannels');
    }

}
