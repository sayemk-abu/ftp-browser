<?php
/**
 * Created by PhpStorm.
 * User: sa
 * Date: 7/11/18
 * Time: 10:50 PM
 */
use Faker\Generator as Faker;

$factory->define(App\MovieCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'status' => 1,

    ];
});