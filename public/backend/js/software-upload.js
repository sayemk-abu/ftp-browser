$(document).ready(function(){
    var r = new Resumable({
        chunkSize: 10 * 1024 * 1024, // 10MB
        simultaneousUploads: 3,
        testChunks: false,
        throttleProgressCallbacks: 1,
        maxFiles:1,
        target:'/admin/software/upload',
        query:{_token : $('input[name=_token]').val()}
    });

    r.assignBrowse(document.getElementById('browseButton'));

    r.on('fileAdded', function(file, event){
        $("#browseUpload").removeClass("hidden");

//                var fileSize = r.getSize();
//                fileSize = (fileSize/1024/1024).toFixed(2) //MB
//
//                $("#file_size").val(fileSize+' MB');

    });
    r.on('fileSuccess', function(file, message){
        console.log(message);
        message = JSON.parse(message);
        $("#file_name").val(message.name);

        $("#browsePause").addClass("hidden");
        $("#browseCancel").addClass("hidden");
        $("#browseUpload").addClass("hidden");
        $("#browseResume").addClass("hidden");
    });
    r.on('fileError', function(file, message){

        $("#browsePause").addClass("hidden");
        $("#browseCancel").addClass("hidden");
        $("#browseUpload").addClass("hidden");
        $("#browseResume").addClass("hidden");
    });
    $("#browseUpload").on("click",function(){
        r.upload();
        $("#progressDiv").removeClass("hidden");
        $("#browsePause").removeClass("hidden");
        $("#browseCancel").removeClass("hidden");
        $("#browseUpload").addClass("hidden");
        setInterval(function() {
            setPogress();
        }, 1000);
    });

    $("#browsePause").on("click",function(){
        r.pause();

        $("#browsePause").attr("disabled","disabled");
        $("#browseResume").removeClass("hidden");

    });

    $("#browseResume").on("click",function(){
        r.upload();
        $("#browsePause").removeAttr("disabled");
        $("#browseResume").addClass("hidden");


    });

    $("#browseCancel").on("click",function(){
        r.cancel();
        $("#progressDiv").addClass("hidden");
        $("#browsePause").addClass("hidden");
        $("#browseCancel").addClass("hidden");
        $("#browseUpload").addClass("hidden");
        $("#browseResume").addClass("hidden");

    });


    function setPogress() {
        var percentage = r.progress();

        percentage = parseInt(percentage*100);

        $("#browseProgress").attr('aria-valuenow',percentage).css("width",percentage+"%").html(percentage+"%");

    }
});