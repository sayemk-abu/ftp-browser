<div class="row">
    <div class="col-md-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <?php
                    $dataId = '';

                ?>
                @foreach(explode('/',$path) as $item)
                        @if ($loop->first)
                            <?php $dataId .= $item; ?>
                        @else
                            <?php $dataId .= '/'.$item; ?>
                        @endif

                        @if ($loop->last)
                                <li class="breadcrumb-item active" aria-current="page">{{ $item }}</li>
                        @else
                            <li class="breadcrumb-item"><a onclick="breadCrumbClick(this)" data-id="{{ $dataId }}" href="#">{{ $item }}</a></li>
                        @endif


                @endforeach

            </ol>
        </nav>
    </div>
</div>
<div class="row">
    <div class="col-md-9 table-responsive" style="overflow-y: scroll; overflow-x: auto; max-height: 724px;">
        <table class="table table-stripped">
            <thead>
            <tr>
                <th style="max-width: 200px">Name</th>
                <th>Last Modified</th>
                <th>Size</th>
            </tr>
            </thead>
            <tbody>
                @foreach($directories as $directory)
                    <tr>
                        <?php $lastModified = \Illuminate\Support\Facades\Storage::disk('ftp')->lastModified($directory) ?>
                        <td>
                            <span title="Double Click to Open Folder" ondblclick="folderDblClick(this)" class="folderClick" data-previous-id="{{ $path }}" data-id="{{ $directory }}" style="cursor: pointer">
                                <i class="fas fa-folder"></i> &nbsp;&nbsp;{{ str_replace($path.'/','',$directory) }}
                            </span>
                        </td>
                        <td>{{ gmdate('Y-m-d H:i:s',$lastModified ) }}</td>
                        <td></td>
                    </tr>
                @endforeach
                @foreach($files as $file)
                    <tr>

                        <td>
                            <a target="_blank" href="{{ url('menu/download').'?file='.base64_encode($file) }}">
                                <i class="fas fa-video"></i> &nbsp;&nbsp;{{ str_replace($path.'/','',$file) }}
                                <br>
                                <span style="padding: 5px 10px" class="btn btn-success btn-sm"><i class="fas fa-download"></i> DownLoad</span>
                            </a>

                        </td>
                        <td>{{ gmdate('Y-m-d H:i:s',\Illuminate\Support\Facades\Storage::disk('ftp')->lastModified($file)) }}</td>
                        <td>
                            {{ \App\Http\FileBrowser::byteToMb(\Illuminate\Support\Facades\Storage::disk('ftp')->size($file)) }}MB
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
    <div class="col-md-3">
        <h1>
            <i class="fas fa-folder"></i>

        </h1>
        <p>
            <strong>{{ $folder }}</strong> <br>
            {{ gmdate('Y-m-d H:i:s',\Illuminate\Support\Facades\Storage::disk('ftp')->lastModified($path) ) }} <br>
            {{ count($directories) }} folders {{ count($files) }} files
        </p>
</div>
</div>