
          
            <nav class="hidden-print" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="" alt="">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                     {!! Form::open(['url' => url('logout'),'class'=>'form-inline']) !!}
                           {!! csrf_field() !!}
                          <li><button class="btn btn-primary btn-lg btn-block register-button" type="submit" >Logout</button> </li>
                       {!! Form::close() !!}
                  </ul>
                </li>

                <li>
                  <a href="{{ url('/') }}" class=" info-number" aria-expanded="false">

                    Visit Website
                  </a>

                </li>
              </ul>
            </nav>