<div class="left_col scroll-view">
  <div class="navbar nav_title" style="border: 0;">
    <a href="{{url('/admin/dashboard')}}" class="site_title"><i class="fa fa-paw"></i> <span>Cinema Bazar</span></a>
  </div>

  <div class="clearfix"></div>

  <!-- menu profile quick info -->
  <div class="profile">
    <div class="profile_pic">
      <img src="/images/content/movie_single/c1.jpg" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
      <span>Welcome, {{ auth()->user()->name }}</span>
      <h2></h2>
    </div>
  </div>
  <!-- /menu profile quick info -->

  <br />

  <!-- sidebar menu -->
  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">
        <li><a href="{{url('/admin/dashboard')}}"> <i class="fa fa-dashboard"></i> Dashboard</a></li>




        {{--<li><a><i class="fa fa-file-movie-o"></i> Movies <span class="fa fa-chevron-down"></span></a>--}}
          {{--<ul class="nav child_menu">--}}
            {{--<li><a href="{{ url('admin/movies') }}">All Movies</a></li>--}}
            {{--<li><a href="{{ url('admin/movies/create') }}">New Movies</a></li>--}}
            {{--<li><a href="{{ url('admin/movie-category') }}">Movie Categories</a></li>--}}
            {{--<li><a href="{{ url('admin/genre') }}">Movie Genres</a></li>--}}

          {{--</ul>--}}
        {{--</li>--}}

        {{--<li><a><i class="fa fa-server"></i> Tv Series <span class="fa fa-chevron-down"></span></a>--}}
          {{--<ul class="nav child_menu">--}}
            {{--<li><a href="{{ url('admin/tvseries') }}">All Series</a></li>--}}
            {{--<li><a href="{{ url('admin/tvseries/create') }}">New Series</a></li>--}}
            {{--<li><a href="{{ url('admin/episode') }}">Episodes</a></li>--}}
            {{--<li><a href="{{ url('admin/tvseriesseason') }}">Seasons</a></li>--}}
            {{--<li><a href="{{ url('admin/tvseriescategory') }}">Categories</a></li>--}}



          {{--</ul>--}}
        {{--</li>--}}

        {{--<li><a><i class="fa fa-files-o"></i> Software <span class="fa fa-chevron-down"></span></a>--}}
          {{--<ul class="nav child_menu">--}}
            {{--<li><a href="{{ url('admin/software') }}">All Software</a></li>--}}
            {{--<li><a href="{{ url('admin/software/create') }}">New Software</a></li>--}}
            {{--<li><a href="{{ url('admin/softwarecategory') }}">Software Categories</a></li>--}}


          {{--</ul>--}}
        {{--</li>--}}

        {{--<li><a><i class="fa fa-motorcycle"></i> Game <span class="fa fa-chevron-down"></span></a>--}}
          {{--<ul class="nav child_menu">--}}
            {{--<li><a href="{{ url('admin/game') }}">All Game</a></li>--}}
            {{--<li><a href="{{ url('admin/game/create') }}">New Game</a></li>--}}

            {{--<li><a href="{{ url('admin/gamecategory') }}">Game Categories</a></li>--}}
            {{--<li><a href="{{ url('admin/gamepublisher') }}">Game Publisher</a></li>--}}
            {{--<li><a href="{{ url('admin/gameplatform') }}">Game Platform</a></li>--}}


          {{--</ul>--}}
        {{--</li>--}}

        <li><a><i class="fa fa-files-o"></i> TV Channels <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{ url('admin/tvchannel') }}">All Channels</a></li>
            <li><a href="{{ url('admin/tvchannel/create') }}">New Channel</a></li>
            <li><a href="{{ url('admin/tvchannelcategory') }}">Categories</a></li>


          </ul>
        </li>

        {{--<li><a><i class="fa fa-sliders"></i> Web Sliders <span class="fa fa-chevron-down"></span></a>--}}
          {{--<ul class="nav child_menu">--}}
            {{--<li><a href="{{ url('admin/webslider') }}">All Sliders</a></li>--}}
            {{--<li><a href="{{ url('admin/webslider/create') }}">New Sliders</a></li>--}}

          {{--</ul>--}}
        {{--</li>--}}
      </ul>
    </div>


  </div>
  <!-- /sidebar menu -->

  <!-- /menu footer buttons -->
  <div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
      <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
      <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout">
      <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
  </div>
  <!-- /menu footer buttons -->
</div>
