@extends('layout.master')
@section('content')
    {{--<div class="prs_main_slider_wrapper">--}}
        {{--<div id="rev_slider_41_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="food-carousel26" data-source="gallery" style="margin:0px auto;padding:0px;margin-top:0px;margin-bottom:0px;">--}}
            {{--<div class="prs_slider_overlay"></div>--}}
            {{--<!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->--}}
            {{--<div id="rev_slider_41_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">--}}
                {{--<ul>--}}
                {{--@foreach(\App\WebSlider::where('status',1)->get() as $slide)--}}
                    {{--<!-- SLIDE  -->--}}
                        {{--<li data-index="rs-145-{{ $slide->id }}" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="The Healthy Bowl" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                            {{--<!-- MAIN IMAGE -->--}}
                            {{--<img src="{{ url('storage/'.$slide->image) }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>--}}
                            {{--<!-- LAYERS -->--}}
                            {{--<!-- LAYER NR. 3 -->--}}
                            {{--<div class="tp-caption FoodCarousel-CloseButton rev-btn  tp-resizeme" id="slide-145-layer-5" data-x="441" data-y="110" data-width="['auto']" data-height="['auto']" data-type="button" data-actions='[{"event":"click","action":"stoplayer","layer":"slide-145-layer-3","delay":""},{"event":"click","action":"stoplayer","layer":"slide-145-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-145-layer-1","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":800,"to":"o:1;","delay":"bytrigger","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"to":"auto:auto;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255,255,255,1);bg:rgba(41,46,49,1);bw:1px 1px 1px 1px;"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[14,14,14,14]" data-paddingright="[14,14,14,14]" data-paddingbottom="[14,14,14,14]" data-paddingleft="[16,16,16,16]" data-lasttriggerstate="reset" style="z-index: 7; white-space: nowrap;border-color:transparent;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-remove"></i>--}}
                            {{--</div>--}}
                            {{--<div class="tp-caption tp-resizeme largewhitebg"--}}

                                 {{--data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 50"},--}}
                   {{--{"delay": "wait", "speed": 300, "to": "opacity: 50"}]'--}}

                                 {{--data-x="center"--}}
                                 {{--data-y="center"--}}
                                 {{--data-hoffset="0"--}}
                                 {{--data-voffset="0"--}}
                                 {{--data-width="['auto']"--}}
                                 {{--data-height="['auto']"--}}

                            {{-->--}}
                                {{--@if($slide->link)--}}
                                    {{--<a href="{{ $slide->link }}">Details</a>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<!-- SLIDE  -->--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
                {{--<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END REVOLUTION SLIDER -->--}}
    {{--</div>--}}
    <!-- prs Slider End -->
    <!-- prs feature slider Start -->
    <div class="prs_feature_slider_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_heading_section_wrapper">
                        <h2>Most Viewed</h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="tab-content">
                        <div id="grid" class="tab-pane fade in active">
                            <?php
                            $row = '<div class="row">';

                            $end = '</div>';
                            $counter = 1;
                            $firstCounter =1;
                            ?>
                            {!! $row !!}
                            @foreach(mostViewed(12) as $item)
                                @if($counter ==5)
                                    {!! $row !!}
                                @endif
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="prs_upcom_movie_box_wrapper prs_mcc_movie_box_wrapper">
                                        <div class="prs_upcom_movie_img_box">
                                            <img style="max-height: 260px; min-height: 260px" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                            <div class="prs_upcom_movie_img_overlay"></div>
                                            <div class="prs_upcom_movie_img_btn_wrapper">
                                                <ul>

                                                    <li><a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">View Details</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="prs_upcom_movie_content_box">
                                            <div class="prs_upcom_movie_content_box_inner">
                                                <h4>
                                                    <a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">
                                                        {{ str_limit($item->name,18,'..') }}
                                                    </a>
                                                </h4>
                                                <?php $genres = movieGenresConcate(\App\Movie::find($item->id)) ?>
                                                <p title="{{ $genres }}">{{ str_limit($genres,'15') }}</p>
                                                <ul>
                                                    <li>
                                                                        <span>
                                                                            Rating: {{ $item->imdb_rating }}
                                                                        </span>
                                                    </li>
                                                    <li>
                                                                        <span>
                                                                            View: {{ $item->views }}
                                                                        </span>
                                                    </li>
                                                </ul>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                                @if($counter ==8)
                                    {!! $end !!}
                                    <?php $counter = 4 ?>
                                @endif

                                @if($firstCounter ==4)
                                    {!! $end !!}

                                @endif
                                <?php
                                $counter++;
                                $firstCounter++
                                ?>

                            @endforeach


                        </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs feature slider End -->

    <!-- Most DownLOad -->
    <div class="prs_feature_slider_main_wrapper" style="padding-top: 0px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_heading_section_wrapper">
                        <h2>Most Downloaded</h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="tab-content">
                        <div id="grid" class="tab-pane fade in active">
                                <?php
                                    $row = '<div class="row">';

                                    $end = '</div>';
                                    $counter = 1;
                                    $firstCounter =1;
                                ?>
                                    {!! $row !!}
                                @foreach(mostDownloaded(12) as $item)
                                    @if($counter ==5)
                                            {!! $row !!}
                                    @endif
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="prs_upcom_movie_box_wrapper prs_mcc_movie_box_wrapper">
                                            <div class="prs_upcom_movie_img_box">
                                                <img style="max-height: 260px; min-height: 260px" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                                <div class="prs_upcom_movie_img_overlay"></div>
                                                <div class="prs_upcom_movie_img_btn_wrapper">
                                                    <ul>

                                                        <li><a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">View Details</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="prs_upcom_movie_content_box">
                                                <div class="prs_upcom_movie_content_box_inner">
                                                    <h4>
                                                        <a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">
                                                            {{ str_limit($item->name,18,'..') }}
                                                        </a>
                                                    </h4>
                                                    <?php $genres = movieGenresConcate(\App\Movie::find($item->id)) ?>
                                                    <p title="{{ $genres }}">{{ str_limit($genres,'15') }}</p>
                                                    <ul>
                                                        <li>
                                                                        <span>
                                                                            Rating: {{ $item->imdb_rating }}
                                                                        </span>
                                                        </li>
                                                        <li>
                                                                        <span>
                                                                            Downloaded: {{ $item->downloads }}
                                                                        </span>
                                                        </li>
                                                    </ul>

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    @if($counter ==8)
                                        {!! $end !!}
                                        <?php $counter = 4 ?>
                                    @endif

                                    @if($firstCounter ==4)
                                        {!! $end !!}

                                    @endif
                                    <?php
                                        $counter++;
                                        $firstCounter++
                                    ?>

                                @endforeach


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Most DownLOad End -->


    <!-- prs theater Slider Start -->
    <div class="prs_theater_main_slider_wrapper">
        <div class="prs_theater_img_overlay"></div>
        <div class="prs_theater_sec_heading_wrapper">
            <h2>Recent Upload</h2>
        </div>
        <div class="wrap-album-slider">
            <ul class="album-slider">
                @foreach(recentUpload(15) as $item)
                    <li class="album-slider__item">
                        <figure class="album">
                            <div class="prs_upcom_movie_box_wrapper">
                                <div class="prs_upcom_movie_img_box">
                                    <img style="max-height: 260px; min-height: 260px" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                    <div class="prs_upcom_movie_img_overlay"></div>
                                    <div class="prs_upcom_movie_img_btn_wrapper">
                                        <ul>
                                            {{--<li><a href="index.html#">View Trailer</a>--}}
                                            {{--</li>--}}
                                            <li><a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">View Details</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="prs_upcom_movie_content_box">
                                    <div class="prs_upcom_movie_content_box_inner">
                                        <h4>
                                            <a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">
                                                {{ str_limit($item->name,17,'..') }}
                                            </a>
                                        </h4>
                                        <p>
                                            {{ movieGenresConcate($item) }} <br>
                                            <span class="color-theme"> Views: </span> {{ $item->views }} &nbsp;&nbsp;
                                            <span class="color-theme"> Downloads: </span> {{ $item->downloads }}
                                        </p>

                                    </div>

                                </div>
                            </div>
                            <!-- End album body -->
                        </figure>
                        <!-- End album -->
                    </li>
                @endforeach

            </ul>
            <!-- End slider -->
        </div>
    </div>


    <!-- prs footer Wrapper Start -->
@endsection
