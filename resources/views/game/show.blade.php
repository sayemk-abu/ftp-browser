@extends('layout.master')
@section('content')

    {{--<!-- prs title wrapper Start -->--}}
    {{--<div class="prs_title_main_sec_wrapper">--}}
    {{--<div class="prs_title_img_overlay"></div>--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
    {{--<div class="prs_title_heading_wrapper">--}}
    {{--<h2>{{ $movie->name }}</h2>--}}
    {{--<ul>--}}
    {{--<li><a href="/">Home</a>--}}
    {{--</li>--}}
    {{--<li>&nbsp;&nbsp; >&nbsp;&nbsp; Movie Details</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

        <!-- prs syn Slider Start -->
    <div class="prs_syn_main_section_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="prs_syn_cont_wrapper prs_upcom_movie_content_box">

                        <p>
                            {!! $game->description !!}
                        </p>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="prs_syn_cont_wrapper prs_upcom_movie_content_box">

                        <img style="width: 100%; max-height: 400px" src="{{ asset('/storage/'.$game->image) }}" alt="{{ $game->name }}">

                        <h2 title="{{ $game->name }}" style="padding-bottom: 40px">
                            {{ $game->name }}
                        </h2>

                        <table class="table">
                            <tr>
                                <th>Category:</th>
                                <th>{{ $game->category->name }}</th>
                            </tr>
                            <tr>
                                <th>Publisher: </th>
                                <th>{{ $game->publisher->name }}</th>
                            </tr>
                            <tr>
                                <th>Platform:</th>
                                <th>{{ gamePlatformConcat($game) }}</th>
                            </tr>
                            <tr>
                                <th>Release On:</th>
                                <th>{{ $game->release_date }}</th>
                            </tr>
                            <tr>
                                <th>File Size</th>
                                <th>{{ $game->file_size }}</th>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    <a class="btn btn-block btn-success" href="{{ route('web.game.download',['id'=>$game->id,'name'=>$game->name]) }}">
                                        Download
                                    </a>
                                </th>

                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs syn Slider End -->

    <!-- prs related movie slider Start -->
    {{--<div class="prs_ms_rm_main_wrapper">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
                    {{--<div class="prs_heading_section_wrapper">--}}
                        {{--<h2>Related Movies</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
                    {{--<div class="prs_ms_rm_slider_wrapper">--}}
                        {{--<div class="owl-carousel owl-theme">--}}
                            {{--@foreach(relatedMovies($movie) as $item)--}}
                                {{--<div class="item">--}}
                                    {{--<div class="prs_upcom_movie_box_wrapper">--}}
                                        {{--<div class="prs_upcom_movie_img_box">--}}
                                            {{--<img style="max-height: 260px; min-height: 260px" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">--}}
                                            {{--<div class="prs_upcom_movie_img_overlay"></div>--}}
                                            {{--<div class="prs_upcom_movie_img_btn_wrapper">--}}
                                                {{--<ul>--}}

                                                    {{--<li><a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">View Details</a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="prs_upcom_movie_content_box">--}}
                                            {{--<div class="prs_upcom_movie_content_box_inner">--}}
                                                {{--<h4>--}}
                                                    {{--<a title="{{ $item->name }}" href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">--}}
                                                        {{--{{ str_limit($item->name,15,'..') }}--}}
                                                    {{--</a>--}}
                                                {{--</h4>--}}
                                                {{--<p>{{ movieGenresConcate($item) }}</p>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- prs related movie slider End -->

    <!-- prs footer Wrapper Start -->
@endsection
