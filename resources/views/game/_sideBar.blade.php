
    <div class="prs_mcc_left_side_wrapper">

        <div class="prs_mcc_bro_title_wrapper">
            <h2>Categories</h2>
            <ul>

                @foreach($categories as $category)
                    <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.game.index') }}?category={{ $category->id }}">
                            {{ $category->name }}<span>{{ $category->gameCount }}</span></a>
                    </li>
                @endforeach

            </ul>
        </div>

        <div class="prs_mcc_bro_title_wrapper">
            <h2>Publishers</h2>
            <ul>

                @foreach($publishers as $category)
                    <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.game.index') }}?publisher={{ $category->id }}">
                            {{ $category->name }}<span>{{ $category->gameCount }}</span></a>
                    </li>
                @endforeach

            </ul>
        </div>

    </div>
