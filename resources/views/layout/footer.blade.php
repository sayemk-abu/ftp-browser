<!--div class="prs_footer_main_section_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="prs_footer_cont1_wrapper prs_footer_cont1_wrapper_1">
                    <h2>LANGUAGE MOVIES</h2>
                    <ul>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">English movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Tamil movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Punjabi Movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Hindi movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Malyalam movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">English Action movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Hindi Action movie</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="prs_footer_cont1_wrapper prs_footer_cont1_wrapper_2">
                    <h2>MOVIES by presenter</h2>
                    <ul>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Action movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Romantic movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Adult movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Comedy movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Drama movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Musical movie</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">Classical movie</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="prs_footer_cont1_wrapper prs_footer_cont1_wrapper_3">
                    <h2>BOOKING ONLINE</h2>
                    <ul>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">www.example.com</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">www.hello.com</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">www.example.com</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">www.hello.com</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">www.example.com</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">www.hello.com</a>
                        </li>
                        <li><i class="fa fa-circle"></i> &nbsp;&nbsp;<a href="index.html#">www.example.com</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="prs_footer_cont1_wrapper prs_footer_cont1_wrapper_4">
                    <h2>App available on</h2>
                    <p>Download App and Get Free Movie Ticket !</p>
                    <ul>
                        <li>
                            <a href="index.html#">
                                <img src="images/content/f1.jpg" alt="footer_img">
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <img src="images/content/f2.jpg" alt="footer_img">
                            </a>
                        </li>
                    </ul>
                    <h5><span>$50</span> Payback on App Download</h5>
                </div>
            </div>
        </div>
    </div>
</div -->

<div class="prs_bottom_footer_wrapper">
        <a href="javascript:" id="return-to-top"><i class="flaticon-play-button"></i></a>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                    <div class="prs_bottom_footer_cont_wrapper">
                        <p>Copyright 2018 <a href="/">Cinema Bazar</a> . All rights reserved - Developed by <a href="https://asteriskbd.com/">AsteriskBD</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                    <div class="prs_footer_social_wrapper">
                        <ul>
                            <li><a href="index.html#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="index.html#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="index.html#"><i class="fa fa-linkedin"></i></a>
                            </li>
                            <li><a href="index.html#"><i class="fa fa-youtube-play"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- prs footer Wrapper End -->
<!--main js file start-->
<script src="/js/jquery_min.js"></script>
<script src="/js/modernizr.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/owl.carousel.js"></script>
<script src="/js/jquery.dlmenu.js"></script>
<script src="/js/jquery.sticky.js"></script>
<script src="/js/jquery.nice-select.min.js"></script>
<script src="/js/jquery.magnific-popup.js"></script>
<script src="/js/jquery.bxslider.min.js"></script>
<script src="/js/venobox.min.js"></script>
<script src="/js/smothscroll_part1.js"></script>
<script src="/js/smothscroll_part2.js"></script>
<script src="/js/plugin/rs_slider/jquery.themepunch.revolution.min.js"></script>
<script src="/js/plugin/rs_slider/jquery.themepunch.tools.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.addon.snow.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.actions.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.carousel.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.kenburn.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.layeranimation.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.migration.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.navigation.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.parallax.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.slideanims.min.js"></script>
<script src="/js/plugin/rs_slider/revolution.extension.video.min.js"></script>
<script src="/js/custom.js"></script>
@yield('script')

