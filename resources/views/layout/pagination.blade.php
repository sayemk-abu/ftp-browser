@if ($paginator->hasPages())

    <ul class="pagination">
        @if ($paginator->onFirstPage())

            <li class="disabled"><a href="#"><i class="flaticon-left-arrow"></i></a> </li>
        @else

            <li>
                <a href="{{ $paginator->previousPageUrl() }}"><i class="flaticon-left-arrow"></i></a>
            </li>
        @endif



        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><a href="#">{{ $element }}</a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><a href="#">{{ $page }}</a>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())

            <li><a href="{{ $paginator->nextPageUrl() }}"><i class="flaticon-right-arrow"></i></a>
            </li>
        @else
            <li class="disabled">
                <a href="#"><i class="flaticon-right-arrow"></i></a>
            </li>
        @endif

    </ul>
@endif
