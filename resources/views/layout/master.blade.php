@include('layout.head')
<body>
<!-- preloader Start -->
<div id="preloader">
    <div id="status">
        <img src="/images/header/horoscope.gif" id="preloader_image" alt="loader">
    </div>
</div>
@include('layout.menu')
@yield('content')

@include('layout.footer')

</body>

</html>