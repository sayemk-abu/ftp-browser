<div class="prs_navigation_main_wrapper">
    <div class="container-fluid">
        <div id="search_open" class="gc_search_box">


            {!! Form::open(['url'=>'movie/search','method'=>'GET']) !!}

            <input type="text" name="movie" placeholder="Search Movie , Video">
            <button type="submit">
                <i class="fa fa-search" aria-hidden="true"></i>

            </button>
            <button type="button" id="search_close">
                    <i class="fa fa-close" aria-hidden="true"></i>
            </button>
            {!! Form::close() !!}
        </div>
        <div class="prs_navi_left_main_wrapper">
            <div class="mobile-logo-custom">
                <img src="{{ asset('images/header/logo-cb.png') }}" alt="Logo">
            </div>
            <div class="prs_menu_main_wrapper">
                <nav class="navbar navbar-default">
                    <div id="dl-menu" class="xv-menuwrapper responsive-menu">
                        <button class="dl-trigger">
                            <img src="/images/header/bars.png" alt="bar_png">
                        </button>
                        <div class="prs_mobail_searchbar_wrapper" id="search_button">	<i class="fa fa-search"></i> </div>

                        <div class="clearfix"></div>
                        <ul class="dl-menu">

                            <li><a href="/">Cinemabazar</a></li>
                            {{--<li class="parent"><a href="/movie">Movies</a>--}}
                                {{--<ul class="lg-submenu">--}}
                                    {{--@foreach(\App\MovieCategory::orderBy('order','ASC')->where('status',1)->get() as $category)--}}
                                        <?php

                                            //$cateYears = getMoviesYear($category->id);
                                            //$count = $cateYears->count();
                                        ?>

                                        {{--<li class="{{ ($count) ? 'parent':'' }}">--}}
                                            {{--<a href="/movie?category={{ $category->id }}">{{ $category->name }}</a>--}}
                                            {{--@if($count)--}}
                                                {{--<ul class="lg-submenu">--}}
                                                    {{--@foreach($cateYears as $yearObj)--}}
                                                        {{--<li>--}}
                                                            {{--<a href="/movie?category={{ $category->id }}&&year={{ $yearObj->year }}">{{ $yearObj->year }}</a>--}}
                                                        {{--</li>--}}
                                                    {{--@endforeach--}}
                                                {{--</ul>--}}
                                            {{--@endif--}}
                                        {{--</li>--}}
                                    {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{ route('web.movie.recent') }}">Recent Upload</a>--}}
                            {{--</li>--}}

                            {{--<li class="parent"><a href="/game">Game</a>--}}
                                {{--<ul class="lg-submenu">--}}
                                    {{--@foreach(\App\GameCategory::orderBy('name','ASC')->where('status',1)->get() as $category)--}}
                                        {{--<li><a href="/game?category={{ $category->id }}">{{ $category->name }}</a></li>--}}
                                    {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</li>--}}

                            {{--<li class="parent"><a href="#">Genre</a>--}}
                                {{--<ul class="lg-submenu">--}}
                                    {{--@foreach(\App\Genre::orderBy('name','ASC')->where('status',1)->get() as $category)--}}
                                        {{--<li><a href="/movie?genre={{ $category->id }}">{{ $category->name }}</a></li>--}}
                                    {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</li>--}}

                            {{--<li class="parent"><a href="/tv-series">TV Series</a>--}}
                                {{--<ul class="lg-submenu">--}}
                                    {{--@foreach(\App\TvSeriesCategory::orderBy('name','ASC')->where('status',1)->get() as $category)--}}
                                        {{--<li><a href="/tv-series?category={{ $category->id }}">{{ $category->name }}</a></li>--}}
                                    {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a href="{{ route('web.software.index') }}">Software</a></li>--}}
                            {{--<li class="parent"><a href="#">Live TV</a>--}}
                                {{--<ul class="lg-submenu">--}}
                                    {{--<li><a href="/live-tv">SK Live</a></li>--}}
                                    {{--<li><a href="https://www.bioscopelive.com/">Bioscope</a></li>--}}

                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">More FTP</a></li>--}}



                        </ul>



                    </div>
                    <!-- /dl-menuwrapper -->
                </nav>
            </div>
        </div>
        <div class="prs_navi_right_main_wrapper">

            {{--<div class="prs_top_login_btn_wrapper">--}}
                {{--<div class="prs_animate_btn1">--}}
                    {{--<ul>--}}
                        {{--@if(auth()->check())--}}
                            {{--<li>--}}
                                {{--<a onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();" href="/logout" class="button button--tamaya" data-text="logout"><span>logout</span></a>--}}
                            {{--</li>--}}
                            {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                {{--@csrf--}}
                            {{--</form>--}}
                        {{--@else--}}
                            {{--<li>--}}
                                {{--<a href="/login" class="button rev-cbutton-light-sr" data-text="sign in"><span>sign in</span></a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="product-heading">--}}
                {{--<div class="con">--}}
                    {{--{!! Form::open(['url'=>'movie/search','method'=>'GET']) !!}--}}
                    {{--{!! Form::select('category',categoryDropdown(),null,[]) !!}--}}

                    {{--{!! Form::text('movie',null,['placeholder'=>'Movie, Video']) !!}--}}
                    {{--<button type="submit"><i class="flaticon-tool"></i>--}}
                    {{--</button>--}}
                    {{--{!! Form::close() !!}--}}
                {{--</div>--}}
            {{--</div>--}}


            {{--<div class="prs_mobail_searchbar_wrapper-2" id="search_button-2">	<i class="fa fa-search"></i> </div>--}}
        </div>

    </div>
</div>