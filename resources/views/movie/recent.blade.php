@extends('layout.master')
@section('content')

    <div class="prs_feature_slider_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_heading_section_wrapper">
                        <h2>Recent Uploaded Movies</h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="tab-content">
                        <div id="grid" class="tab-pane fade in active">
                            <?php
                            $row = '<div class="row">';

                            $end = '</div>';
                            $counter = 1;
                            $firstCounter =1;
                            ?>
                            {!! $row !!}
                            @foreach($movies as $item)
                                @if($counter ==5)
                                    {!! $row !!}
                                @endif
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="prs_upcom_movie_box_wrapper prs_mcc_movie_box_wrapper">
                                        <div class="prs_upcom_movie_img_box">
                                            <img style="max-height: 260px; min-height: 260px" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                            <div class="prs_upcom_movie_img_overlay"></div>
                                            <div class="prs_upcom_movie_img_btn_wrapper">
                                                <ul>

                                                    <li><a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">View Details</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="prs_upcom_movie_content_box">
                                            <div class="prs_upcom_movie_content_box_inner">
                                                <h4>
                                                    <a href="{{ route('web.movie.show',['id'=>$item->id,'name'=>$item->name]) }}">
                                                        {{ str_limit($item->name,18,'..') }}
                                                    </a>
                                                </h4>
                                                <?php $genres = movieGenresConcate(\App\Movie::find($item->id)) ?>
                                                <p title="{{ $genres }}">{{ str_limit($genres,'15') }}</p>
                                                <ul>
                                                    <li>
                                                                        <span>
                                                                            Rating: {{ $item->imdb_rating }}
                                                                        </span>
                                                    </li>
                                                    <li>
                                                                        <span>
                                                                            View: {{ $item->views }}
                                                                        </span>
                                                    </li>
                                                </ul>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                                @if($counter ==8)
                                    {!! $end !!}
                                    <?php $counter = 4 ?>
                                @endif

                                @if($firstCounter ==4)
                                    {!! $end !!}

                                @endif
                                <?php
                                $counter++;
                                $firstCounter++
                                ?>

                            @endforeach


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- prs feature slider End -->




    <!-- prs footer Wrapper Start -->
@endsection
