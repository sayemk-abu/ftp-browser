
    <div class="prs_mcc_left_side_wrapper">
        <div class="prs_mcc_event_title_wrapper">
            @if($topMovie)
                <h2>Top Movie</h2>
                <a href="{{ route('web.movie.show',['id'=>$topMovie->id,'name'=>$topMovie->name]) }}">
                    <img style="max-height: 200px; min-height: 200px" src="{{ url('storage/'.$topMovie->image) }}" alt="{{ $topMovie->name }}">
                </a>
                <h3><a title="{{ $topMovie->name }}" href="{{ route('web.movie.show',['id'=>$topMovie->id,'name'=>$topMovie->name]) }}">{{ str_limit($topMovie->name,18,'..') }}</a></h3>

                <h4>
                    IMDB Rating:
                    <span>
                     {{ $topMovie->imdb_rating }}
                </span> <br>
                    Viewed:
                    <span>
                     {{ $topMovie->views }}
                </span>
                    <br>
                    Download:
                    <span>
                     {{ $topMovie->downloads }}
                </span>
                </h4>
            @endif
        </div>
        <div class="prs_mcc_bro_title_wrapper">
            <h2>Browse Category</h2>
            <ul>
                <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.movie') }}">All <span>{{ $totalMovies }}</span></a>
                </li>
                @foreach($categories as $category)
                    <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.movie') }}?category={{ $category->id }}">
                            {{ $category->name }}<span>{{ $category->movieCounts }}</span></a>
                    </li>
                @endforeach

            </ul>

            <h2>Browse Years</h2>
            <ul>

                @foreach($releaseYears as $years)
                    <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.movie') }}? {{ $yearUrl }}year={{ $years->year }}">
                            {{ $years->year }}</a>
                    </li>
                @endforeach

            </ul>
        </div>

    </div>
