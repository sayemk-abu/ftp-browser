@extends('backLayout.app')
@section('title')
Movie Details
@stop
@section('style')
    <link href="https://vjs.zencdn.net/7.0.5/video-js.css" rel="stylesheet">
@endsection
@section('content')

    <h1>
        Movie
        <a class="btn btn-default" href="{{ url('/admin/movies/') }}">Back List</a>
        <a class="btn btn-warning" href="{{ url()->route('admin.movies.edit',['id'=>$movie->id]) }}">Edit</a>
    </h1>
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <tr>
                        <th>Name</th>
                        <th>{{ $movie->name }}</th>
                    </tr>
                    <tr>
                        <th>Category</th>
                        <th>{{ $movie->category->name }}</th>
                    </tr>
                    <tr>
                        <th>Genres</th>
                        <th>
                            {{ movieGenresConcate($movie) }}
                        </th>
                    </tr>
                    <tr>
                        <th>File Size</th>
                        <th>{{ $movie->file_size }}</th>
                    </tr>
                    <tr>
                        <th>IMDB Rating</th>
                        <th>{{ $movie->imdb_rating }}</th>
                    </tr>
                    <tr>
                        <th>Video Quality</th>
                        <th>{{ $movie->video_quality }}</th>
                    </tr>
                    <tr>
                        <th>File Type</th>
                        <th>{{ $movie->file_type }}</th>
                    </tr>
                    <tr>
                        <th>Duration</th>
                        <th>{{ $movie->duration }}</th>
                    </tr>
                    <tr>
                        <th>Language</th>
                        <th>{{ $movie->language }}</th>
                    </tr>
                    <tr>
                        <th>Trailer Link</th>
                        <th>{{ $movie->trailer_link }}</th>
                    </tr>
                    <tr>
                        <th>Release Date</th>
                        <th>{{ $movie->release_date }}</th>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th> <span class="text-danger">{{ activeStatus($movie->status) }}</span></th>
                    </tr>
                    <tr>
                        <th>Image</th>
                        <th>
                            <img class=" img-thumbnail img-w-250" src="{{ asset('storage/'.$movie->image) }}" alt="{{ $movie->name }}">
                        </th>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <h3>Movie</h3>
            <video style="max-width: 70%" controls src="{{ asset('storage/'.$movie->file_location) }}"></video>
            @if(!empty($movie->trailer_link))
                <h3>Trailer</h3>
                <video style="max-width: 70%" controls src="{{ $movie->trailer_link }}"></video>
            @endif

            {{--<video style="max-width: 70%; max-height: 240px" id="my-video" class="video-js" controls preload="auto"--}}
                   {{--poster="{{ asset('storage/'.$movie->image) }}" data-setup="{}">--}}
                {{--<source src="{{ asset('storage/'.$movie->file_location) }}">--}}

                {{--<p class="vjs-no-js">--}}
                    {{--To view this video please enable JavaScript, and consider upgrading to a web browser that--}}
                    {{--<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>--}}
                {{--</p>--}}
            {{--</video>--}}
        </div>
    </div>

@endsection


@section('scripts')
    <script src="https://vjs.zencdn.net/7.0.5/video.js"></script>

    <script>
        $(document).ready(function() {
//            var player = videojs('my-video', {
//
//            });

        });
    </script>
@endsection