@extends('backLayout.app')
@section('title')
Movie
@stop

@section('content')

    <h1>Movies <a href="{{ url('admin/movies/create') }}" class="btn btn-primary pull-right btn-sm">Add New Movie</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>File Size</th>
                    <th>IMDB Rating</th>
                    <th>Release Date</th>
                    <th>Status</th>

                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($movies as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ @$item->category->name }}</td>
                    <td>
                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->name }}">
                    </td>
                    <td>{{ $item->file_size }}</td>
                    <td>{{ $item->imdb_rating }}</td>
                    <td>{{ $item->release_date }}</td>
                    <td>{{ activeStatus($item->status) }}</td>

                    <td>
                        <a href="{{ url('admin/movies', $item->id) }}" class="btn btn-success btn-xs">View</a>
                        <a href="{{ url('admin/movies/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/movies', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({
            columnDefs: [{

                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection