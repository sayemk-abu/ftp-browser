@extends('backLayout.app')
@section('title')
Edit Movie
@stop
@section('style')
    <link rel="stylesheet" href="{{ URL::asset('/backend/vendors/select2/dist/css/select2.min.css') }}">
@endsection
@section('content')

    <h1>Edit Movie</h1>
    <hr/>

    {!! Form::model($movie, [
        'method' => 'PATCH',
        'url' => ['admin/movies', $movie->id],
        'class' => 'form-horizontal',
        'files' =>true
    ]) !!}
            @include('backEnd.movies._file_browse')
            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('category_id', categoryDropdown(),null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('genre_id') ? 'has-error' : ''}}">
                {!! Form::label('genre_id', 'Genre: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('genre_id[]', genreDropdown(), $movie->genres->pluck('id'), ['class' => 'form-control', 'required' => 'required','multiple'=>'multiple','id'=>'genre_id']) !!}
                    {!! $errors->first('genre_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('file_size') ? 'has-error' : ''}}">
                {!! Form::label('file_size', 'File Size: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('file_size', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('file_size', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('imdb_rating') ? 'has-error' : ''}}">
                {!! Form::label('imdb_rating', 'Imdb Rating: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('imdb_rating', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('imdb_rating', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('video_quality') ? 'has-error' : ''}}">
                {!! Form::label('video_quality', 'Video Quality: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('video_quality', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('video_quality', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('file_type') ? 'has-error' : ''}}">
                {!! Form::label('file_type', 'File Type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('file_type', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('file_type', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('file_location') ? 'has-error' : ''}}">
                {!! Form::label('file_location', 'File Location: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('file_location', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('file_location', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
                {!! Form::label('duration', 'Duration: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('language') ? 'has-error' : ''}}">
                {!! Form::label('language', 'Language: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('language', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('language', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            {{--<div class="form-group {{ $errors->has('trailer_link') ? 'has-error' : ''}}">--}}
                {{--{!! Form::label('trailer_link', 'Trailer Link: ', ['class' => 'col-sm-3 control-label']) !!}--}}
                {{--<div class="col-sm-6">--}}
                    {{--{!! Form::text('trailer_link', null, ['class' => 'form-control']) !!}--}}
                    {{--{!! $errors->first('trailer_link', '<p class="help-block">:message</p>') !!}--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', ['class' => 'form-control']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$movie->image) }}" alt="{{ $movie->name }}">
                </div>
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('release_date') ? 'has-error' : ''}}">
                {!! Form::label('release_date', 'Release Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('release_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            {{--<div class="form-group {{ $errors->has('subtitle_link') ? 'has-error' : ''}}">--}}
                {{--{!! Form::label('subtitle_link', 'Subtitle Link: ', ['class' => 'col-sm-3 control-label']) !!}--}}
                {{--<div class="col-sm-6">--}}
                    {{--{!! Form::text('subtitle_link', null, ['class' => 'form-control']) !!}--}}
                    {{--{!! $errors->first('subtitle_link', '<p class="help-block">:message</p>') !!}--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('status', '1',true) !!} Publish</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('status', '0') !!} Unpublish</label>
            </div>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}


@endsection


@section('scripts')
    <script src="{{ URL::asset('/backend/vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/resumeable/resumeable.js') }}"></script>
    <script src="{{ URL::asset('/backend/js/movie-upload.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#genre_id').select2({
                placeholder: 'Select Genres'
            });
            $("#duration").inputmask("hh:mm:ss", {
                    placeholder: "HH:MM:SS",
                    insertMode: false,
                    showMaskOnHover: true,
                    hourFormat: 12
                }
            );

            $("#release_date").inputmask("date", {
                    //placeholder: "yyyy-mm-dd",
                    insertMode: false,
                    showMaskOnHover: true,
                    //hourFormat: 12
                }
            );

        });
    </script>
@endsection