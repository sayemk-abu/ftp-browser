@extends('backLayout.app')
@section('title')
    Tv Series
@stop

@section('content')

    <h1>Tv Series <a href="{{ url('admin/tvseries/create') }}" class="btn btn-primary pull-right btn-sm">Add New Tv Series</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Genre</th>
                    <th>Release date</th>
                    <th>Image</th>
                    <th>views</th>
                    <th>Download</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tvseries as $item)
                <tr>

                    <td>{{ $item->name }}</td>
                    <td>{{ $item->category->name }}</td>
                    <td>{{ tvSeriesGenresConcat($item) }}</td>
                    <td>{{ $item->release_date }}</td>
                    <td>
                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->name }}">
                    </td>
                    <td>{{ $item->views }}</td>
                    <td>{{ $item->downloads }}</td>
                    <td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/tvseries/' . $item->id ) }}" class="btn btn-success btn-xs">View</a>
                        <a href="{{ url('admin/tvseries/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/tvseries', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                        <a href="{{ url('admin/episode?').'tvseries='.$item->id }}" class="btn btn-success btn-xs">Episodes</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

        });
    });
</script>
@endsection