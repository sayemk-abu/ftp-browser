@extends('backLayout.app')
@section('title')
    Tv Series
@stop

@section('content')

    <h1>Tv Series
        <a class="btn btn-default" href="{{ route('admin.tvseries.index') }}">Series List</a>
        <a class="btn btn-warning" href="{{ route('admin.tvseries.edit',['id'=>$tvseries->id]) }}">Edit</a>
    </h1>
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">

                    <tbody>
                    <tr>
                        <th>Name</th>
                        <th>{{ $tvseries->name }}</th>
                    </tr>
                    <tr>
                        <th>Category</th>
                        <th>{{ $tvseries->category->name }}</th>
                    </tr>
                    <tr>
                        <th>Genres</th>
                        <th>{{ tvSeriesGenresConcat($tvseries) }}</th>
                    </tr>
                    <tr>
                        <th>Release Date</th>
                        <th>{{ $tvseries->release_date }}</th>
                    </tr>
                    <tr>
                        <th>Views</th>
                        <th>{{ $tvseries->views }}</th>
                    </tr>
                    <tr>
                        <th>Downloads</th>
                        <th>{{ $tvseries->downloads }}</th>
                    </tr>
                    @if($tvseries->has_season)
                        <tr>
                            <th>Sessons</th>
                            <th>{{ tvSeriesSessonConcat($tvseries) }}</th>
                        </tr>

                    @endif
                    @if($tvseries->has_episode)
                        <tr>
                            <th>Total Episode</th>
                            <th>{{ $tvseries->episodes()->count() }}</th>
                        </tr>

                    @endif
                    <tr>
                        <th>Description</th>
                        <th>{!! $tvseries->description !!}</th>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <th>{{ activeStatus($tvseries->status) }}</th>
                    </tr>
                    <tr>
                        <th>Image</th>
                        <th>
                            <img class=" img-thumbnail img-w-250" src="{{ asset('storage/'.$tvseries->image) }}" alt="{{ $tvseries->name }}">
                        </th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            @if($tvseries->has_episode ==0 && $tvseries->has_season==0)
                <h3>Video </h3>
                <video style="max-width: 70%" controls src="{{ asset('storage/tv-series/'.$tvseries->file_name) }}"></video>
            @endif

        </div>
    </div>

@endsection