@extends('backLayout.app')
@section('title')
Edit Tv Series
@stop
@section('style')
    <link rel="stylesheet" href="{{ URL::asset('/backend/vendors/select2/dist/css/select2.min.css') }}">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@endsection

@section('content')

    <h1>Edit Tv Series</h1>
    <hr/>

    {!! Form::model($tvseries, [
        'method' => 'PATCH',
        'url' => ['admin/tvseries', $tvseries->id],
        'class' => 'form-horizontal',
        'files'=>true
    ]) !!}

    @include('backEnd.tvseries._file_browse')
    <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
        {!! Form::label('category_id', 'Category: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('category_id', tvSeriesCategoryDropdown(),null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('genre_id') ? 'has-error' : ''}}">
        {!! Form::label('genre_id', 'Genre: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('genre_id[]', genreDropdown(), old('genre_id',$tvseries->genres->pluck('id')), ['class' => 'form-control', 'required' => 'required','multiple'=>'multiple','id'=>'genre_id']) !!}
            {!! $errors->first('genre_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div id="fileNameDiv" class="form-group hidden {{ $errors->has('file_name') ? 'has-error' : ''}}">
        {!! Form::label('file_name', 'File Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('file_name', null, ['class' => 'form-control']) !!}
            {!! $errors->first('file_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('release_date') ? 'has-error' : ''}}">
        {!! Form::label('release_date', 'Release Date: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('release_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
        {!! Form::label('image', 'Image: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::file('image', ['class' => 'form-control']) !!}
            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
            <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$tvseries->image) }}" alt="{{ $tvseries->name }}">
        </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('has_season') ? 'has-error' : ''}}">
        {!! Form::label('has_season', 'Has Season: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <div class="checkbox">
                <label>{!! Form::radio('has_season', '1',true) !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('has_season', '0') !!} No</label>
            </div>
            {!! $errors->first('has_season', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('has_episode') ? 'has-error' : ''}}">
        {!! Form::label('has_episode', 'Has Episode: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <div class="checkbox">
                <label>{!! Form::radio('has_episode', '1',true) !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('has_episode', '0') !!} No</label>
            </div>
            {!! $errors->first('has_episode', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <div class="checkbox">
                <label>{!! Form::radio('status', '1', true) !!} Publish</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('status', '0') !!} Unpublish</label>
            </div>
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}


@endsection


@section('scripts')
    <script src="{{ URL::asset('/backend/vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/resumeable/resumeable.js') }}"></script>
    <script src="{{ URL::asset('/backend/js/tv-series.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <script>
        $(document).ready(function() {
            $('#genre_id').select2({
                placeholder: 'Select Genre'
            });


            $("#release_date").inputmask("date", {
                    //placeholder: "yyyy-mm-dd",
                    insertMode: false,
                    showMaskOnHover: true,
                    //hourFormat: 12
                }
            );

            $('#description').summernote();
            var has_episode = {{ old('has_episode',$tvseries->has_episode) }};
            var has_season = {{old('has_season',$tvseries->has_season)}};
            setFileBrowser(has_episode, has_season);
            $('input:radio[name="has_episode"], input:radio[name="has_season"]').on('click',function(event){
                has_episode = $('input:radio[name="has_episode"]:checked').val();
                has_season = $('input:radio[name="has_season"]:checked').val();
                setFileBrowser(has_episode, has_season);
            });

        });

        function setFileBrowser(episode,seasson)
        {
            if(episode ==0 && seasson==0)
            {

                $('#fileNameDiv').removeClass('hidden');
                $('#fileBrowserDiv').removeClass('hidden');
                $('input[name="file_present"]').val(1);
            }else {

                $('#fileNameDiv').addClass('hidden');
                $('#fileBrowserDiv').addClass('hidden');
                $('input[name="file_present"]').removeAttr('value');
            }
        }
    </script>
@endsection