<div id="fileBrowserDiv" class="form-group hidden">
    {!! Form::label('File', 'Select Movie File: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        <span class="btn btn-success" id="browseButton">Browse</span>
        <span class="btn btn-success hidden" id="browseUpload">Upload</span>
        <span class="btn btn-warning hidden" id="browsePause">Pause</span>
        <span class="btn btn-primary hidden" id="browseResume">Resume</span>
        <span class="btn btn-danger hidden" id="browseCancel">Cancel</span>

        <div class="progress hidden" id="progressDiv">
            <div class="progress-bar progress-bar-success" id="browseProgress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
                0%
            </div>
        </div>
    </div>

</div>