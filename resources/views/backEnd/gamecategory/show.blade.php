@extends('backLayout.app')
@section('title')
Gamecategory
@stop

@section('content')

    <h1>Gamecategory</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $gamecategory->id }}</td> <td> {{ $gamecategory->name }} </td><td> {{ $gamecategory->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection