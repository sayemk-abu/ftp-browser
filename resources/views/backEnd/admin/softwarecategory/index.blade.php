@extends('backLayout.app')
@section('title')
    Software Category
@stop

@section('content')

    <h1>Software Category <a href="{{ url('admin/softwarecategory/create') }}" class="btn btn-primary pull-right btn-sm">Add New Software Category</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Title</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($softwarecategory as $item)
                <tr>

                    <td><a href="{{ url('admin/softwarecategory', $item->id) }}">{{ $item->title }}</a></td><td>{{ $item->status }}</td>
                    <td>
                        <a href="{{ url('admin/softwarecategory/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/softwarecategory', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

            order: [[0, "asc"]],
        });
    });
</script>
@endsection