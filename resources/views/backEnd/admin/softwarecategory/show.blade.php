@extends('backLayout.app')
@section('title')
    Software Category
@stop

@section('content')

    <h1>Software Category</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Title</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $softwarecategory->id }}</td> <td> {{ $softwarecategory->title }} </td><td> {{ $softwarecategory->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection