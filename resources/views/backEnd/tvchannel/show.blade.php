@extends('backLayout.app')
@section('title')
    Tv Channel
@stop

@section('content')

    <h1>Tv Channel</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Category Id</th><th>Name</th><th>Url</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $tvchannel->id }}</td> <td> {{ $tvchannel->category_id }} </td><td> {{ $tvchannel->name }} </td><td> {{ $tvchannel->url }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection