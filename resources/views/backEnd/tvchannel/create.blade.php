@extends('backLayout.app')
@section('title')
Create new Tv Channel
@stop

@section('content')

    <h1>Create New Tv Channel</h1>
    <hr/>

    {!! Form::open(['url' => 'admin/tvchannel', 'class' => 'form-horizontal', 'files' => true]) !!}

                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('category_id', tvChannelCategoryDropDown(), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
                {!! Form::label('url', 'Url: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('url', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
    <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
        {!! Form::label('image', 'Image: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::file('image'  , ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <div class="checkbox">
                <label>{!! Form::radio('status', '1') !!} Publish</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('status', '0', true) !!} Unpublish</label>
            </div>
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection