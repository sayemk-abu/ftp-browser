@extends('backLayout.app')
@section('title')
    Tv Channel
@stop

@section('content')

    <h1>Tv Channel <a href="{{ url('admin/tvchannel/create') }}" class="btn btn-primary pull-right btn-sm">Add New Tv Channel</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category Id</th>
                    <th>Image</th>
                    <th>Url</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tvchannel as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ @$item->category->name }}</td>
                    <td>
                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->name }}">
                    </td>
                    <td>{{ $item->url }}</td>
                    <td>
                        <a href="{{ url('admin/tvchannel/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/tvchannel', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

            order: [[0, "asc"]],
        });
    });
</script>
@endsection