
@extends('backLayout.app')
@section('title')
DashBoard
@stop

@section('style')

@stop
@section('content')


    <div class="">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-download"></i></div>
                    <div class="count">{{ $totalMovies }}</div>
                    <h3>Total Movies</h3>

                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-download"></i></div>
                    <div class="count">{{ $totalTvSeries }}</div>
                    <h3>Total TV Series</h3>

                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-file-movie-o"></i></div>
                    <div class="count">{{ $totalSoftware }}</div>
                    <h3>Total Softwares</h3>

                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-download"></i></div>
                    <div class="count">{{ $totalGame }}</div>
                    <h3>Total Games</h3>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Top Movies <small>Views</small></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                        <table class="table table-striped   ">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>

                                <th>Views</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($topMovies as $movie)
                                    <tr>
                                        <td>
                                            <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$movie->image) }}" alt="{{ $movie->name }}">
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/movies', $movie->id) }}">
                                                {{ $movie->name }}
                                            </a>
                                        </td>

                                        <td>
                                            {{ $movie->views }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Top Tv Series <small>Views</small></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                        <table class="table table-striped   ">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>

                                <th>Views</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($topSeries as $movie)
                                <tr>
                                    <td>
                                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$movie->image) }}" alt="{{ $movie->name }}">
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/tvseries', $movie->id) }}">
                                            {{ $movie->name }}
                                        </a>
                                    </td>

                                    <td>
                                        {{ $movie->views }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Top Software <small>Downloads</small></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                        <table class="table table-striped   ">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>

                                <th>Downloads</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($topSoftware as $movie)
                                <tr>
                                    <td>
                                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$movie->image) }}" alt="{{ $movie->name }}">
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/software', $movie->id) }}">
                                            {{ $movie->name }}
                                        </a>
                                    </td>

                                    <td>
                                        {{ $movie->download }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')


@endsection