@extends('backLayout.app')
@section('title')
Genre
@stop

@section('content')

    <h1>Genre</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $genre->id }}</td> <td> {{ $genre->name }} </td><td> {{ $genre->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection