@extends('backLayout.app')
@section('title')
Genre
@stop

@section('content')

    <h1>Genre <a href="{{ url('admin/genre/create') }}" class="btn btn-primary pull-right btn-sm">Add New Genre</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($genres as $item)
                <tr>

                    <td>{{ $item->name }}</td>
                    <td>{!! activeStatus($item->status) !!}</td>
                    <td>
                        <a href="{{ url('admin/genre/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({
            columnDefs: [{

                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection