@extends('backLayout.app')
@section('title')
Movie-Category
@stop

@section('content')

    <h1>Movie-category <a href="{{ url('admin/movie-category/create') }}" class="btn btn-primary pull-right btn-sm">Add New Category</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th><th>Order</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($categories as $item)
                <tr>

                    <td>{{ $item->name }}</td>
                    <td>{{ $item->order }}</td>
                    <td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/movie-category/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({
           ordering:false
        });
    });
</script>
@endsection