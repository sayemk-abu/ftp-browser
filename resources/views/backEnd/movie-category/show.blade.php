@extends('backLayout.app')
@section('title')
Movie-category
@stop

@section('content')

    <h1>Movie-category</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $movie-category->id }}</td> <td> {{ $movie-category->name }} </td><td> {{ $movie-category->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection