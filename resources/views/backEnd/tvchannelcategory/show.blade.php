@extends('backLayout.app')
@section('title')
    Channel Category
@stop

@section('content')

    <h1>Channel Category</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $tvchannelcategory->id }}</td> <td> {{ $tvchannelcategory->name }} </td><td> {{ $tvchannelcategory->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection