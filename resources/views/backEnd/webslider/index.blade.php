@extends('backLayout.app')
@section('title')
Webslider
@stop

@section('content')

    <h1>Webslider <a href="{{ url('admin/webslider/create') }}" class="btn btn-primary pull-right btn-sm">Add New Webslider</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Image</th><th>LinK</th><th>Position</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($webslider as $item)
                <tr>

                    <td>
                        <a href="{{ url('admin/webslider', $item->id) }}">
                            <img class="thumbnail img-w-150 img-h-150" src="{{ url('storage').'/'.$item->image }}" alt="">
                        </a>
                    </td>
                    <td>{{ $item->link }}</td>
                    <td>{{ $item->position }}</td>
                    <td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/webslider/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/webslider', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({
            columnDefs: [{
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection