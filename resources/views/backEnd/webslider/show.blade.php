@extends('backLayout.app')
@section('title')
Webslider
@stop

@section('content')

    <h1>Webslider
        <a class="btn btn-default" href="{{ url('/admin/webslider/') }}">Back List</a>
        <a class="btn btn-warning" href="{{ url()->route('admin.webslider.edit',['id'=>$webslider->id]) }}">Edit</a>
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <th>Link (URL)</th>
                <th>
                    <a href="{{ $webslider->link }}"> {{ $webslider->link }} </a>
                </th>
            </tr>
            <tr>
                <th>Position</th>
                <th>{{ $webslider->position }}</th>
            </tr>
            <tr>
                <th>Status</th>
                <th>{{ activeStatus($webslider->status) }}</th>
            </tr>
            <tr>
                <th>Image</th>
                <th>
                    <img class="thumbnail" src="{{ url('storage').'/'.$webslider->image }}" alt="">
                </th>
            </tr>  
        </table>
    </div>

@endsection