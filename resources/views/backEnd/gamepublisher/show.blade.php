@extends('backLayout.app')
@section('title')
Gamepublisher
@stop

@section('content')

    <h1>Gamepublisher</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $gamepublisher->id }}</td> <td> {{ $gamepublisher->name }} </td><td> {{ $gamepublisher->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection