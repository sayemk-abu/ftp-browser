@extends('backLayout.app')
@section('title')
Gamepublisher
@stop

@section('content')

    <h1>Gamepublisher <a href="{{ url('admin/gamepublisher/create') }}" class="btn btn-primary pull-right btn-sm">Add New Gamepublisher</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($gamepublisher as $item)
                <tr>

                    <td>{{ $item->name }}</td><td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/gamepublisher/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {{--{!! Form::open([--}}
                            {{--'method'=>'DELETE',--}}
                            {{--'url' => ['admin/gamepublisher', $item->id],--}}
                            {{--'style' => 'display:inline'--}}
                        {{--]) !!}--}}
                            {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}--}}
                        {{--{!! Form::close() !!}--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

        });
    });
</script>
@endsection