@extends('backLayout.app')
@section('title')
Software
@stop

@section('content')

    <h1>Software</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Category Id</th><th>Name</th><th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $software->id }}</td> <td> {{ $software->category_id }} </td><td> {{ $software->name }} </td><td> {{ $software->description }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection