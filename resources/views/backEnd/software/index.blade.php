@extends('backLayout.app')
@section('title')
Software
@stop

@section('content')

    <h1>Software <a href="{{ url('admin/software/create') }}" class="btn btn-primary pull-right btn-sm">Add New Software</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($software as $item)
                <tr>


                    <td>{{ $item->name }}</td>
                    <td>{{ $item->category->title }}</td>
                    <td>
                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->name }}">
                    </td>
                    <td>{{ $item->description }}</td>
                    <td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/software/' . $item->id) }}" class="btn btn-success btn-xs">View</a>
                        <a href="{{ url('admin/software/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/software', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

            order: [[0, "asc"]],
        });
    });
</script>
@endsection