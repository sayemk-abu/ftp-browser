@extends('backLayout.app')
@section('title')
Edit Tv Series Season
@stop
@section('style')
    <link rel="stylesheet" href="{{ URL::asset('/backend/vendors/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')

    <h1>Edit Tv Series Season</h1>
    <hr/>

    {!! Form::model($tvseriesseason, [
        'method' => 'PATCH',
        'url' => ['admin/tvseriesseason', $tvseriesseason->id],
        'class' => 'form-horizontal'
    ]) !!}


    <div class="form-group {{ $errors->has('tv_sesries_id') ? 'has-error' : ''}}">
        {!! Form::label('tv_sesries_id', 'Tv Sesries Id: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('tv_sesries_id',tvSeriesDropDownForSeason(), null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('tv_sesries_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('release_date') ? 'has-error' : ''}}">
        {!! Form::label('release_date', 'Release Date: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('release_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <div class="checkbox">
                <label>{!! Form::radio('status', '1',true) !!} Publish</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('status', '0') !!} Unpublish</label>
            </div>
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}



@endsection



@section('scripts')
    <script src="{{ URL::asset('/backend/vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>



    <script>
        $(document).ready(function() {
            $('#tv_sesries_id').select2({
                placeholder: 'Select Series'
            });
            $("#release_date").inputmask("date", {
                    //placeholder: "yyyy-mm-dd",
                    insertMode: false,
                    showMaskOnHover: true,
                    //hourFormat: 12
                }
            );

        });


    </script>
@endsection