@extends('backLayout.app')
@section('title')
    Tv Series Season
@stop

@section('content')

    <h1>Tv Series Season
        <a class="btn btn-default" href="{{ url('admin/tvseriesseason') }}">All Seasons </a>
        <a class="btn btn-warning" href="{{ url('admin/tvseriesseason'.$tvseriesseason->id.'/edit') }}"> Edit Season</a>
        <a class="btn btn-info" href="{{ route('admin.episode.index',['season'=>$tvseriesseason->id]) }}"> View Episodes</a>
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">

            <tbody>
            <tr>
                <th>Name</th>
                <th>{{ $tvseriesseason->name }}</th>
            </tr>
            <tr>
                <th>Tv Series</th>
                <th>{{ $tvseriesseason->tvSeries->name }}</th>
            </tr>
            <tr>
                <th>Release Date</th>
                <th>{{ $tvseriesseason->release_date }}</th>
            </tr>
            <tr>
                <th>Status</th>
                <th>{{ activeStatus($tvseriesseason->status) }}</th>
            </tr>
            <tr>
                <th>Episodes</th>
                <th>
                    Total Episodes {{ $tvseriesseason->episodes()->count() }}

                </th>
            </tr>
            </tbody>    
        </table>
    </div>

@endsection