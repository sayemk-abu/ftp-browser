@extends('backLayout.app')
@section('title')
    Tv Series Season
@stop

@section('content')

    <h1>Tv Series Season <a href="{{ url('admin/tvseriesseason/create') }}" class="btn btn-primary pull-right btn-sm">Add New Tv Series Season</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Tv Series</th>
                    <th>Release Date</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tvseriesseason as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->tvSeries->name }}</td>
                    <td>{{ $item->release_date }}</td>
                    <td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/tvseriesseason/' . $item->id ) }}" class="btn btn-success btn-xs">View</a>
                        <a href="{{ url('admin/tvseriesseason/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/tvseriesseason', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}

                        <a class="btn btn-info btn-xs" href="{{ route('admin.episode.index',['season'=>$item->id]) }}"> View Episode</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

        });
    });
</script>
@endsection