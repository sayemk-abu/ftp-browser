@extends('backLayout.app')
@section('title')
Game
@stop

@section('content')

    <h1>Game
        <a class="btn btn-default" href="{{ route('admin.game.index') }}">All Games</a>
        <a class="btn btn-warning" href="{{ route('admin.game.edit',['id'=>$game->id]) }}">Edit</a>
    </h1>
    <div class="row">
        <div class="col-md-8">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <th>{{ $game->name }}</th>
                    </tr>
                    <tr>
                        <th>Category</th>
                        <th>{{ $game->category->name }}</th>
                    </tr>
                    <tr>
                        <th>Publisher</th>
                        <th>{{ $game->publisher->name }}</th>
                    </tr>
                    <tr>
                        <th>Platform</th>
                        <th>{{ gamePlatformConcat($game) }}</th>
                    </tr>
                    <tr>
                        <th>Release On</th>
                        <th>{{ $game->release_date }}</th>
                    </tr>
                    <tr>
                        <th>File Size</th>
                        <th>{{ $game->file_size }}</th>
                    </tr>
                    <tr>
                        <th>File Name</th>
                        <th>{{ $game->file_location }}</th>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>{{ activeStatus($game->status) }}</th>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <th>{!! $game->description !!}</th>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <img class="img-responsive img-thumbnail" src="{{ asset('storage/'.$game->image) }}" alt="">
        </div>
    </div>

@endsection