@extends('backLayout.app')
@section('title')
Game
@stop

@section('content')

    <h1>Game <a href="{{ url('admin/game/create') }}" class="btn btn-primary pull-right btn-sm">Add New Game</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Publisher</th>
                    <th>Release On</th>
                    <th>File Size</th>
                    <th>Image</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($game as $item)
                <tr>

                    <td>{{ str_limit($item->name,40,'..') }}</td>
                    <td>{{ $item->category->name }}</td>
                    <td>{{ $item->publisher->name }}</td>
                    <td>{{ $item->release_date }}</td>
                    <td>{{ $item->file_size }}</td>
                    <td>
                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->name }}">
                    </td>
                    <td>
                        <a href="{{ url('admin/game/' . $item->id ) }}" class="btn btn-success btn-xs">View</a>
                        <a href="{{ url('admin/game/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/game', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

        });
    });
</script>
@endsection