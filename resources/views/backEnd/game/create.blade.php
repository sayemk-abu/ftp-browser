@extends('backLayout.app')
@section('title')
Create new Game
@stop

@section('style')
    <link rel="stylesheet" href="{{ URL::asset('/backend/vendors/select2/dist/css/select2.min.css') }}">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@endsection

@section('content')

    <h1>Create New Game</h1>
    <hr/>

    {!! Form::open(['url' => 'admin/game', 'class' => 'form-horizontal','files'=>true]) !!}
            @include('backEnd.movies._file_browse')
            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('category_id',gameCategoryDropdown(), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('publisher_id') ? 'has-error' : ''}}">
                {!! Form::label('publisher_id', 'Publisher: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('publisher_id',gamePublisherDropdown(), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('publisher_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('platform') ? 'has-error' : ''}}">
                {!! Form::label('platform', 'Platform: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('platform[]',gamePlatformDropdown(), null, ['id'=>'platForm','class' => 'form-control','multiple'=>true, 'required' => 'required']) !!}
                    {!! $errors->first('platform', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('file_location') ? 'has-error' : ''}}">
                {!! Form::label('file_location', 'File Location: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('file_location', null, ['class' => 'form-control', 'required' => 'required','readonly']) !!}
                    {!! $errors->first('file_location', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('file_size') ? 'has-error' : ''}}">
                {!! Form::label('file_size', 'File Size: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('file_size', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('file_size', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('release_date') ? 'has-error' : ''}}">
                {!! Form::label('release_date', 'Release Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('release_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['col'=>'10','class' => 'form-control','id'=>'game-description']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('status', '1') !!} Publish</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('status', '0', true) !!} Unpublish</label>
                    </div>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}


@endsection


@section('scripts')
    <script src="{{ URL::asset('/backend/vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/resumeable/resumeable.js') }}"></script>
    <script src="{{ URL::asset('/backend/js/game-upload.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <script>
        $(document).ready(function() {
            $('#platForm').select2({
                placeholder: 'Select Platforms'
            });


            $("#release_date").inputmask("date", {
                    //placeholder: "yyyy-mm-dd",
                    insertMode: false,
                    showMaskOnHover: true,
                    //hourFormat: 12
                }
            );

            $('#game-description').summernote();

        });
    </script>
@endsection