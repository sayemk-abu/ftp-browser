@extends('backLayout.app')
@section('title')
Create new Episode
@stop
@section('style')
    <link rel="stylesheet" href="{{ URL::asset('/backend/vendors/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')

    <h1>Create New Episode</h1>
    <hr/>

    {!! Form::open(['url' => 'admin/episode', 'class' => 'form-horizontal','files'=>true]) !!}
            @include('backEnd.movies._file_browse')
            {!! Form::hidden('has_season',1) !!}
            <div class="form-group {{ $errors->has('tvseries_id') ? 'has-error' : ''}}">
                {!! Form::label('tvseries_id', 'Tv Series: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('tvseries_id',tvSeriesDropDown(), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('tvseries_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('season_id') ? 'has-error' : ''}}">
                {!! Form::label('season_id', 'Season: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('season_id',[], null, ['class' => 'form-control']) !!}
                    {!! $errors->first('season_id', '<p class="help-block">:message</p>') !!}
                    <p id="seasonNoNeed" class="help-block text-success"></p>
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('release_date') ? 'has-error' : ''}}">
                {!! Form::label('release_date', 'Release Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('release_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('file_name') ? 'has-error' : ''}}">
                {!! Form::label('file_name', 'File Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('file_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('file_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('status', '1') !!} Publish</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('status', '0', true) !!} Unpublish</label>
                    </div>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}


@endsection

@section('scripts')
    <script src="{{ URL::asset('/backend/vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/resumeable/resumeable.js') }}"></script>
    <script src="{{ URL::asset('/backend/js/tv-series.js') }}"></script>
    <script src="{{ URL::asset('/backend/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#tvseries_id').select2({
                placeholder: 'Select Genre'
            });

            $('#season_id').select2({
                placeholder: 'Select Season'
            });


            $("#release_date").inputmask("date", {
                    //placeholder: "yyyy-mm-dd",
                    insertMode: false,
                    showMaskOnHover: true
                    //hourFormat: 12
                }
            );


            $('#tvseries_id').on('select2:select', function (e) {
                var data = e.params.data;
                var seasonSelect = $('#season_id');
                $.ajax({
                    url: '{{ url('admin/tvseries/season') }}'+'/'+data.id,
                    type:'GET'
                }).success(function(data){
                    console.log(data);
                    if(data.has_season==1)
                    {
                        $.each(data.seasons, function(key,value){
                            console.log(key);
                            var option = new Option(value, key, true, true);
                            seasonSelect.append(option);
                        });

                        seasonSelect.trigger('change');
                        $('input[name="has_season"]').val(1);

                    }else {
                        $('#seasonNoNeed').html('No need to select Season');
                        $('input[name="has_season"]').removeAttr('value');
                    }
                }).fail(function(data){
                    console.log(data);
                })
            });

        });

    </script>
@endsection