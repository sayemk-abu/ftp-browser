@extends('backLayout.app')
@section('title')
Episode
@stop

@section('content')

    <h1>Episode <a href="{{ url('admin/episode/create') }}" class="btn btn-primary pull-right btn-sm">Add New Episode</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Series</th>
                    <th>Season</th>
                    <th>Image</th>
                    <th>Release Date</th>
                    <th>Views</th>
                    <th>Downloads</th>
                    <th>Status</th>

                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($episode as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->tvseries->name }}</td>
                    <td>{{ ($item->season_id) ? $item->season->name : '' }}</td>
                    <td>
                        <img class=" img-thumbnail img-w-150" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->name }}">
                    </td>
                    <td>{{ $item->release_date }}</td>
                    <td>{{ $item->views }}</td>
                    <td>{{ $item->downloads }}</td>
                    <td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/episode/' . $item->id) }}" class="btn btn-success btn-xs">View</a>
                        <a href="{{ url('admin/episode/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/episode', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

        });
    });
</script>
@endsection