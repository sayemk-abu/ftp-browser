@extends('backLayout.app')
@section('title')
Episode
@stop

@section('content')

    <h1>Episode
        <a href="{{ url('admin/episode/') }}" class="btn btn-default btn-xs">Episode List</a>
        <a href="{{ url('admin/episode/' . $episode->id . '/edit') }}" class="btn btn-primary btn-xs">Edit</a>
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">

            <tbody>
            <tr>
                <th>Name</th>
                <td>{{ $episode->name }}</td>
            </tr>
            <tr>
                <th>Series</th>
                <td>{{ $episode->tvseries->name }}</td>
            </tr>
            <tr>
                <th>Season</th>
                <td>{{ ($episode->season_id) ? $episode->season->name : '' }}</td>
            </tr>
            <tr>
                <th>Release Date</th>
                <td>{{ $episode->release_date }}</td>
            </tr>
            <tr>
                <th>Views</th>
                <td>{{ $episode->views }}</td>
            </tr>
            <tr>
                <th>Downloads</th>
                <td>{{ $episode->downloads }}</td>
            </tr>
                <tr>
                    <th>Status</th>





                    <td>{{ activeStatus($episode->status) }}</td>

                </tr>
            <tr>
                <th>Image</th>
                <th>
                    <img class=" img-thumbnail" src="{{ asset('storage/'.$episode->image) }}" alt="{{ $episode->name }}">
                </th>
            </tr>

            </tbody>
        </table>
    </div>

@endsection