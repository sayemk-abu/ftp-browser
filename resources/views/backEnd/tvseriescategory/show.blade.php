@extends('backLayout.app')
@section('title')
    Tv Series Category
@stop

@section('content')

    <h1>Tv Series Category</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   <td> {{ $tvseriescategory->name }} </td><td> {{ $tvseriescategory->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection