@extends('backLayout.app')
@section('title')
    Tv Series Category
@stop

@section('content')

    <h1>Tv Series Category <a href="{{ url('admin/tvseriescategory/create') }}" class="btn btn-primary pull-right btn-sm">Add New Tv Series Category</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tvseriescategory as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td><td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/tvseriescategory/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

        });
    });
</script>
@endsection