@extends('backLayout.app')
@section('title')
Gameplatform
@stop

@section('content')

    <h1>Gameplatform <a href="{{ url('admin/gameplatform/create') }}" class="btn btn-primary pull-right btn-sm">Add New Gameplatform</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin">
            <thead>
                <tr>
                    <th>Name</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($gameplatform as $item)
                <tr>

                    <td>{{ $item->name }}</td><td>{{ activeStatus($item->status) }}</td>
                    <td>
                        <a href="{{ url('admin/gameplatform/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {{--{!! Form::open([--}}
                            {{--'method'=>'DELETE',--}}
                            {{--'url' => ['admin/gameplatform', $item->id],--}}
                            {{--'style' => 'display:inline'--}}
                        {{--]) !!}--}}
                            {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}--}}
                        {{--{!! Form::close() !!}--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin').DataTable({

        });
    });
</script>
@endsection