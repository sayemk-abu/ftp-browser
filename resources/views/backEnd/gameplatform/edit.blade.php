@extends('backLayout.app')
@section('title')
Edit Gameplatform
@stop

@section('content')

    <h1>Edit Gameplatform</h1>
    <hr/>

    {!! Form::model($gameplatform, [
        'method' => 'PATCH',
        'url' => ['admin/gameplatform', $gameplatform->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
    <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <div class="checkbox">
                <label>{!! Form::radio('status', '1') !!} Publish</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('status', '0', true) !!} Unpublish</label>
            </div>
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection