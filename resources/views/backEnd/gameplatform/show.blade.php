@extends('backLayout.app')
@section('title')
Gameplatform
@stop

@section('content')

    <h1>Gameplatform</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $gameplatform->id }}</td> <td> {{ $gameplatform->name }} </td><td> {{ $gameplatform->status }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection