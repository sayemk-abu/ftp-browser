@extends('layout.master')
@section('content')
    <div class="prs_title_main_sec_wrapper">
        <div class="prs_title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_title_heading_wrapper">
                        <h2>Admin Login</h2>
                        <ul>
                            <li><a href="/">Home</a>
                            </li>
                            <li>&nbsp;&nbsp; >&nbsp;&nbsp; Login</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs title wrapper End -->
    <!-- prs mc slider wrapper Start -->
    <div class="prs_mc_slider_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_heading_section_wrapper">
                        <h2>Login</h2>
                        <div class="col-lg-3 col-md-3"></div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                            {!! Form::open(['url'=>'login','method']) !!}
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="prs_contact_input_wrapper text-left {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input type="text" name="email" placeholder="Email">

                                        @if ($errors->has('email'))
                                            <span id="helpBlock" class="help-block text-danger">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="prs_contact_input_wrapper text-left {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input type="password" name="password" placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span id="helpBlock" class="help-block text-danger">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class=" prs_contact_input_wrapper2">
                                        <ul>
                                            <li>
                                                {!! Form::submit('Login',['class'=>'btn btn-danger btn-lg btn-block']) !!}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-lg-3 col-md-3"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
