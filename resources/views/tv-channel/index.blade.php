@extends('layout.master')
@section('content')


    <!-- prs ms trailer wrapper Start -->
    <div class="prs_ms_trailer_vid_main_wrapper" style="padding-top: 50px; background:#ffffff;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-lg-12 col-lg-12 col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs" role="tablist">

                                @foreach($categories as $category)

                                    <li role="presentation" class="{{ ($loop->first) ? 'active' : '' }}" >
                                        <a href="#season{{$category->id}}" aria-controls="season-{{$category->id}}" role="tab" data-toggle="tab">{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div style="min-height: 720px" class="tab-content">
                        @foreach($categories as $category)
                            <div id="season{{$category->id}}" class="tab-pane {{ ($loop->first) ? ' fade in active' : '' }}" role="tabpanel">
                                <div class="row">
                                    @foreach($category->channels as $item)

                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 padding-15-all">

                                            <a href="{{ route('web.tv-channel.watch',['id'=>$item->id,'name'=>$item->url]) }}">
                                                <img class="img-thumbnail" style="max-height: 100px; min-height: 140px; max-width: 100%; min-width: 100%" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                            </a>

                                        </div>
                                    @endforeach
                                </div>


                            </div>

                    @endforeach

                </div>

            </div>
        </div>
    </div>
    </div>
    <!-- prs ms trailer wrapper End -->


    <!-- prs footer Wrapper Start -->
@endsection
