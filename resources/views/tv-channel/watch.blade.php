@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{ asset('js/plugin/plyr/plyr.css') }}">
@endsection
@section('content')

    <div class="prs_syn_main_section_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">
                    <video id="movie-player" style="max-width: 100%; min-height: 100%; " controls
                    >

                    </video>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-lg-12 col-lg-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" role="tablist">

                                        @foreach($categories as $category)

                                            <li role="presentation" class="{{ ($category->id == $channel->category_id) ? 'active' : '' }}" >
                                                <a href="#season{{$category->id}}" aria-controls="season-{{$category->id}}" role="tab" data-toggle="tab">{{ $category->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-content">
                                @foreach($categories as $category)
                                    <div id="season{{$category->id}}" class="tab-pane {{ ($category->id == $channel->category_id) ? ' fade in active' : '' }}" role="tabpanel">
                                        <div class="row">
                                            @foreach($category->channels as $item)

                                                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6 padding-15-all">

                                                    <a href="{{ route('web.tv-channel.watch',['id'=>$item->id,'name'=>$item->url]) }}">
                                                        <img class="img-thumbnail" style="max-height: 85px; min-height: 85px; max-width: 100%; min-width: 100%" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                                    </a>

                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- prs title wrapper End -->
    <!-- prs ms trailer wrapper Start -->

@endsection

@section('script')
    <script src="{{ asset('js/plugin/plyr/plyr.min.js') }}"></script>
    <script src="{{ asset('js/plugin/plyr/hls.js') }}"></script>
    <script>
        $(document).ready(function(){


                const source = '{{ $serverUrl }}';
                const video = document.getElementById('movie-player');

                // For more options see: https://github.com/sampotts/plyr/#options
                // captions.update is required for captions to work with hls.js
                const player = new Plyr(video, {
                    controls: [
                        'play-large',
                        'play',

                        'mute',
                        'volume',
                        'captions',
                        'settings',
                        'pip',
                        'airplay',
                        'fullscreen'
                    ]
                });

                if (!Hls.isSupported()) {
                    video.src = source;
                } else {
                    // For more Hls.js options, see https://github.com/dailymotion/hls.js
                    const hls = new Hls();
                    hls.loadSource(source);
                    hls.attachMedia(video);
                    window.hls = hls;

                    // Handle changing captions

            }
            player.play();

            // Expose player so it can be used from the console
            window.player = player;


        })
    </script>
@endsection


