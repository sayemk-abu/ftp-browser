
    <div class="prs_mcc_left_side_wrapper">

        <div class="prs_mcc_bro_title_wrapper">
            <h2>Browse Category</h2>
            <ul>
                <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.software.index') }}">All <span>{{ $totalSoft }}</span></a>
                </li>
                @foreach($categories as $category)
                    <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.software.index') }}?category={{ $category->id }}">
                            {{ $category->title }}<span>{{ $category->softCount }}</span></a>
                    </li>
                @endforeach

            </ul>
        </div>

    </div>
