@extends('layout.master')
@section('content')

    {{--<!-- prs title wrapper Start -->--}}
    {{--<div class="prs_title_main_sec_wrapper">--}}
        {{--<div class="prs_title_img_overlay"></div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
                    {{--<div class="prs_title_heading_wrapper">--}}
                        {{--<h2>{{ $pageTitle }}</h2>--}}
                        {{--<ul>--}}
                            {{--<li><a href="/">Home</a>--}}
                            {{--</li>--}}
                            {{--<li>&nbsp;&nbsp; >&nbsp;&nbsp; Software</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <!-- prs title wrapper End -->
    <!-- prs mc category slidebar Start -->
    <div class="prs_mc_category_sidebar_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    @include('software._sideBar')
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="prs_mcc_right_side_wrapper">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="prs_mcc_right_side_heading_wrapper">
                                    <h2>{{ $pageTitle }}</h2>

                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tab-content">
                                    <div id="grid" class="tab-pane fade in active">
                                        <div class="row">
                                            @forelse($softwares as $item)
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 prs_upcom_slide_first">
                                                    <div class="prs_upcom_movie_box_wrapper prs_mcc_movie_box_wrapper">
                                                        <div class="prs_upcom_movie_img_box">
                                                            <img style="max-height: 260px; min-height: 260px" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                                            <div class="prs_upcom_movie_img_overlay"></div>
                                                            <div class="prs_upcom_movie_img_btn_wrapper">
                                                                <ul>

                                                                    <li><a href="{{ route('web.software.download',['id'=>$item->id,'name'=>$item->name]) }}">Download</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="prs_upcom_movie_content_box">
                                                            <div class="prs_upcom_movie_content_box_inner">

                                                                <h5>
                                                                    <a href="{{ route('web.software.download',['id'=>$item->id,'name'=>$item->name]) }}">
                                                                        {{ str_limit($item->name,18,'..') }}
                                                                    </a>
                                                                </h5>


                                                                <ul>
                                                                    <li>
                                                                        <span>
                                                                           {{ str_limit($item->category->title,20,'..') }}
                                                                        </span>
                                                                    </li>
                                                                    {{--<li style="padding-top: 15px">--}}

                                                                            {{--<a class="btn btn-danger extra-padding" href="{{ route('web.software.download',['id'=>$item->id,'name'=>$item->name]) }}">--}}
                                                                                {{--Download--}}
                                                                            {{--</a>--}}

                                                                    {{--</li>--}}
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            @empty
                                                <h2>No Software Match Your Search Criteria </h2>
                                            @endforelse

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="pager_wrapper gc_blog_pagination">
                                                    {!! $softwares->links("layout.pagination") !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 visible-sm visible-xs">
                    {{--@include('movie._sideBar')--}}
                </div>
            </div>
        </div>
    </div>
    <!-- prs mc category slidebar End -->

    <!-- prs related movie slider Start -->
    <div class="prs_ms_rm_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_heading_section_wrapper">
                        <h2>Recent Upload</h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_ms_rm_slider_wrapper">
                        <div class="owl-carousel owl-theme">
                            @foreach(recentUploadSoftware(15) as $item)
                                <div class="item">
                                    <div class="prs_upcom_movie_box_wrapper">
                                        <div class="prs_upcom_movie_img_box">
                                            <img src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                            <div class="prs_upcom_movie_img_overlay"></div>
                                            <div class="prs_upcom_movie_img_btn_wrapper">
                                                <ul>

                                                    <li><a href="{{ route('web.software.download',['id'=>$item->id,'name'=>$item->name]) }}">Download</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="prs_upcom_movie_content_box">
                                            <div class="prs_upcom_movie_content_box_inner">
                                                <h4><a href="{{ route('web.software.download',['id'=>$item->id,'name'=>$item->name]) }}">{{ $item->name }}</a></h4>
                                                <ul>
                                                    <li>
                                                        <span>
                                                           {{ str_limit($item->category->title,20,'..') }}
                                                        </span>
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs related movie slider End -->

    <!-- prs footer Wrapper Start -->
@endsection
