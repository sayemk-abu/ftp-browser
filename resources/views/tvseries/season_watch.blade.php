@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{ asset('js/plugin/plyr/plyr.css') }}">
@endsection
@section('content')

    <div class="prs_syn_main_section_wrapper">
        <div class="container">

            <div class="row prs_ms_trailer_slider_main_wrapper">

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs 12">
                    <video id="movie-player" style="max-width: 100%; min-height: 100%; " controls src="{{ asset('storage/tv-series/'.$episode->file_name) }}"
                           poster="{{ url('storage/'.$episode->image) }}"
                    >

                    </video>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-bottom: 15px">
                    <h3 title="{{ $series->name }}">{{ str_limit($episode->name,18,'..') }}</h3>
                    <p>
                        <span class="color-theme">Released on: </span> &nbsp;{{ dateReadable($episode->release_date) }}
                        <br>

                        <span class="color-theme">Views: </span> &nbsp;{{ $episode->views }} <br>
                        <span class="color-theme">Downloads: </span> &nbsp;{{ $episode->downloads }} <br>
                    </p>
                    <br>
                    <span>
                        <a class="btn btn-success btn-lg" href="{{ route('web.series.download',['id'=>$series->id,'name'=>$series->name,'episode'=>$episode->id]) }}">Download</a>

                    </span>
                </div>

            </div>
        </div>
    </div>
    <!-- prs title wrapper End -->
    <!-- prs ms trailer wrapper Start -->
    <div class="prs_ms_trailer_vid_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-lg-12 col-lg-12 col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs" role="tablist">

                                @foreach($series->seasons as $season)

                                    <li role="presentation" class="{{ ($season->id==$episode->season_id) ? 'active' : '' }}" >
                                        <a href="#season{{$season->id}}" aria-controls="season-{{$season->id}}" role="tab" data-toggle="tab">{{ $season->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        @foreach($series->seasons as $season)
                            <div id="season{{$season->id}}" class="tab-pane {{ ($season->id==$episode->season_id) ? ' fade in active' : '' }}" role="tabpanel">
                                <?php
                                $row = '<div class="row">';

                                $end = '</div>';
                                $counter = 1;
                                $firstCounter =1;
                                ?>
                                {!! $row !!}
                                @foreach($season->episodes as $item)
                                    @if($counter ==5)
                                        {!! $row !!}
                                    @endif
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="prs_upcom_movie_box_wrapper prs_mcc_movie_box_wrapper">
                                            <div class="prs_upcom_movie_img_box">
                                                <img style="max-height: 260px; min-height: 260px" src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                                <div class="prs_upcom_movie_img_overlay"></div>
                                                <div class="prs_upcom_movie_img_btn_wrapper">
                                                    <ul>

                                                        <li>
                                                            <a href="{{ route('web.series.watch',['id'=>$series->id,'name'=>$season->id,'episode'=>$item->id]) }}">View</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="prs_upcom_movie_content_box">
                                                <div class="prs_upcom_movie_content_box_inner">
                                                    <h4>
                                                        <a href="{{ route('web.series.watch',['id'=>$series->id,'name'=>$season->id,'episode'=>$item->id]) }}">
                                                            {{ str_limit($item->name,18,'..') }}
                                                        </a>
                                                    </h4>

                                                    <ul>

                                                        <li>
                                                                        <span>
                                                                            Views: {{ $item->views }}
                                                                        </span>
                                                        </li>
                                                    </ul>

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    @if($counter ==8)
                                        {!! $end !!}
                                        <?php $counter = 4 ?>
                                    @endif

                                    @if($firstCounter ==4)
                                        {!! $end !!}

                                    @endif
                                    <?php
                                    $counter++;
                                    $firstCounter++
                                    ?>

                                @endforeach
                            </div>
                    </div>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
    </div>
    <!-- prs ms trailer wrapper End -->

    <!-- prs syn Slider Start -->

    <!-- prs syn Slider End -->

    <!-- prs related movie slider Start -->
    <div class="prs_ms_rm_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_heading_section_wrapper">
                        <h2>Related Movies</h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_ms_rm_slider_wrapper">
                        <div class="owl-carousel owl-theme">
                            @foreach(tvSeriesRelated($series) as $item)
                                <div class="item">
                                    <div class="prs_upcom_movie_box_wrapper">
                                        <div class="prs_upcom_movie_img_box">
                                            <img src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                            <div class="prs_upcom_movie_img_overlay"></div>
                                            <div class="prs_upcom_movie_img_btn_wrapper">
                                                <ul>

                                                    <li><a href="{{ route('web.series.show',['id'=>$item->id,'name'=>$item->name]) }}">View Details</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="prs_upcom_movie_content_box">
                                            <div class="prs_upcom_movie_content_box_inner">
                                                <h4><a href="{{ route('web.series.show',['id'=>$item->id,'name'=>$item->name]) }}">{{ $item->name }}</a></h4>
                                                <ul>
                                                    <li>
                                                        <span>
                                                            Views: {{ $item->views }}
                                                        </span>
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs related movie slider End -->

    <!-- prs footer Wrapper Start -->
@endsection

@section('script')
    <script src="{{ asset('js/plugin/plyr/plyr.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            const player = new Plyr('#movie-player', {
                /* options */
            });
            console.log(player.media)

        })
    </script>
@endsection

