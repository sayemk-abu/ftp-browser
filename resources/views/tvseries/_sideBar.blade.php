
    <div class="prs_mcc_left_side_wrapper">

        <div class="prs_mcc_bro_title_wrapper">
            <h2>Categories</h2>
            <ul>
                <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.series.index') }}">
                        All<span>{{ $totalSeries }}</span></a>
                </li>
                @foreach($categories as $category)
                    <li><i class="fa fa-caret-right"></i> &nbsp;&nbsp;&nbsp;<a href="{{ route('web.series.index') }}?category={{ $category->id }}">
                            {{ $category->name }}<span>{{ $category->seriesCounts }}</span></a>
                    </li>
                @endforeach

            </ul>
        </div>


    </div>
