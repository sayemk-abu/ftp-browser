@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{ asset('js/plugin/plyr/plyr.css') }}">
@endsection
@section('content')

    {{--<!-- prs title wrapper Start -->--}}
    {{--<div class="prs_title_main_sec_wrapper">--}}
    {{--<div class="prs_title_img_overlay"></div>--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
    {{--<div class="prs_title_heading_wrapper">--}}
    {{--<h2>{{ $movie->name }}</h2>--}}
    {{--<ul>--}}
    {{--<li><a href="/">Home</a>--}}
    {{--</li>--}}
    {{--<li>&nbsp;&nbsp; >&nbsp;&nbsp; Movie Details</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    <!-- prs title wrapper End -->
    <!-- prs ms trailer wrapper Start -->
    <div class="prs_ms_trailer_vid_main_wrapper">
        <div class="container">
            <div class="row prs_ms_trailer_slider_main_wrapper">

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs 12">
                    <video id="movie-player" style="max-width: 100%; min-height: 100%; " controls src="{{ asset('storage/tv-series/'.$series->file_name) }}"
                           poster="{{ url('storage/'.$series->image) }}"
                    >

                    </video>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-bottom: 15px">
                    <h3 title="{{ $series->name }}">{{ $series->name }}</h3>
                    <p>
                        <span class="color-theme">Released on: </span> &nbsp;{{ dateReadable($series->release_date) }}
                        <br>

                        <span class="color-theme">Views: </span> &nbsp;{{ $series->views }} <br>
                        <span class="color-theme">Downloads: </span> &nbsp;{{ $series->downloads }} <br>
                    </p>
                    <br>
                    <span>
                        <a class="btn btn-success btn-lg" href="{{ route('web.series.download',['id'=>$series->id,'name'=>$series->name]) }}">Download</a>

                    </span>
                </div>

            </div>

        </div>
    </div>
    <!-- prs ms trailer wrapper End -->

    <!-- prs syn Slider Start -->
    <div class="prs_syn_main_section_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="prs_syn_cont_wrapper">
                        <h3>Synopsis</h3>
                        <h4><span>Category -</span> {{ $series->category->name }}</h4>
                        <h4><span>Genre -</span> {{ tvSeriesGenresConcat($series) }}</h4>
                        <p>
                            {!! $series->description !!}
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="prs_syn_img_wrapper">
                        <img src="{{ url('storage/'.$series->image) }}" alt="{{ $series->name }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs syn Slider End -->

    <!-- prs related movie slider Start -->
    <div class="prs_ms_rm_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_heading_section_wrapper">
                        <h2>Related Movies</h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prs_ms_rm_slider_wrapper">
                        <div class="owl-carousel owl-theme">
                            @foreach(tvSeriesRelated($series) as $item)
                                <div class="item">
                                    <div class="prs_upcom_movie_box_wrapper">
                                        <div class="prs_upcom_movie_img_box">
                                            <img src="{{ url('storage/'.$item->image) }}" alt="{{ $item->name }}">
                                            <div class="prs_upcom_movie_img_overlay"></div>
                                            <div class="prs_upcom_movie_img_btn_wrapper">
                                                <ul>

                                                    <li><a href="{{ route('web.series.show',['id'=>$item->id,'name'=>$item->name]) }}">View Details</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="prs_upcom_movie_content_box">
                                            <div class="prs_upcom_movie_content_box_inner">
                                                <h4><a href="{{ route('web.series.show',['id'=>$item->id,'name'=>$item->name]) }}">{{ $item->name }}</a></h4>
                                                <ul>
                                                    <li>
                                                        <span>
                                                            Views: {{ $item->views }}
                                                        </span>
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs related movie slider End -->

    <!-- prs footer Wrapper Start -->
@endsection

@section('script')
    <script src="{{ asset('js/plugin/plyr/plyr.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            const player = new Plyr('#movie-player', {
                /* options */
            });
            console.log(player.media)

        })
    </script>
@endsection

